<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\core\api_controller;
use App\Models\api\menu_model;
use Illuminate\Http\Request;

class menu extends api_controller
{
    function __construct()
    {
        $this->menu_model = new menu_model;
    }

    public function profile()
    {
        if (!session()->get('sess_user')) {
            return redirect('lainnya');
        }
        return view('lainnya/menu/profile');
    }

    public function get_usaha()
    {
        if (!session()->get('sess_user')) {
            return redirect('lainnya');
        }
        $data = $this->menu_model->get_usaha(session()->get('sess_user')['id']);
        return view('lainnya/menu/usaha')->with(['data' => $data]);
    }

    public function get_promo()
    {
        if (!session()->get('sess_user')) {
            return redirect('lainnya');
        }
        $data = $this->menu_model->get_promo(session()->get('sess_user')['id']);
        // $data = $this->menu_model->get_promo(14);
        return view('lainnya/menu/promo')->with(['data' => $data]);
    }
    public function get_peluang()
    {
        if (!session()->get('sess_user')) {
            return redirect('lainnya');
        }
        $data = $this->menu_model->get_peluang(session()->get('sess_user')['id']);
        return view('lainnya/menu/peluang')->with(['data' => $data]);
    }


    public function edit_usaha($id)
    {
        if (!session()->get('sess_user')) {
            return redirect('lainnya');
        }
        $data = $this->menu_model->get_usaha_detail(session()->get('sess_user')['id'], $id);
        // dd($data);
        return view('lainnya/menu/edit_usaha')->with(['data' => $data]);
    }

    public function edit_promo($id)
    {
        if (!session()->get('sess_user')) {
            return redirect('lainnya');
        }
        $data = $this->menu_model->get_promo_detail(session()->get('sess_user')['id'], $id);
        // dd($data);
        return view('lainnya/menu/edit_promo')->with(['data' => $data]);
    }
}
