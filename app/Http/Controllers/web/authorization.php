<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\core\web_controller;
use App\Models\api\auth_model;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Auth;

class authorization extends web_controller
{
    function __construct()
    {
        $this->authorization = new auth_model();
    }

    public function login(Request $request)
    {
    }
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
    public function handleProviderCallback(Request $request)
    {
        try {
            $user_google = Socialite::driver('google')->user();
            $data = [
                'name' => $user_google->name,
                'email' => $user_google->email,
                'image' => $user_google->avatar
            ];
            $check = (array) $this->authorization->send_login($data);
            $request->session()->put('sess_user', $check);
            return redirect('lainnya');
        } catch (\Exception $e) {
            return redirect('lainnya');
        }
    }

    public function destroySession(Request $request)
    {
        $request->session()->pull('sess_user');
        return redirect('lainnya');
    }
}
