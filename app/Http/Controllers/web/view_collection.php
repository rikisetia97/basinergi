<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\core\web_controller;
use App\Models\api\blog_model;
use Illuminate\Http\Request;
use App\Models\api\usaha_model;
use App\Models\api\dashboard_model;
use App\Models\api\promo_model;
use App\Models\api\peluang_model;
use App\Models\api\member_model;

class view_collection extends web_controller
{
    function __construct()
    {
        $this->usaha_model = new usaha_model;
        $this->promo_model = new promo_model();
        $this->peluang_model = new peluang_model();
        $this->dashboard_model = new dashboard_model;
        $this->blog_model = new blog_model();
        $this->member_model = new member_model();
    }

    public function home()
    {
        $all_usaha = $this->dashboard_model->get_all_usaha();
        $banners = $this->dashboard_model->get_banners();
        $kategori = $this->usaha_model->get_all_kategori();
        $usaha_baru = $this->dashboard_model->get_usaha_baru();
        $promo_baru = $this->dashboard_model->get_promo_baru();
        $peluang_baru = $this->dashboard_model->get_peluang_baru();
        $blog_baru = $this->dashboard_model->get_blog_baru();
        return view('home/home')->with([
            'all_usaha' => $all_usaha,
            'banners' => $banners,
            'kategori' => $kategori,
            'usaha_baru' => $usaha_baru,
            'promo_baru' => $promo_baru,
            'peluang_baru' => $peluang_baru,
            'blog_baru' => $blog_baru,
        ]);
    }

    public function usaha()
    {
        $data = $this->usaha_model->get_all_kategori();
        return view('usaha/usaha')->with(['kategori' => $data]);
    }
    public function blog()
    {
        $data = $this->blog_model->get_all_kategori();
        return view('blog/blog')->with(['kategori' => $data]);
    }
    public function usaha_detail($slug)
    {
        $data = $this->usaha_model->get_detail_usaha($slug);
        return view('usaha/detail')->with(['usaha' => $data]);
    }
    public function promo_detail($slug)
    {
        $data = $this->promo_model->get_detail_promo($slug);
        return view('promo/detail')->with(['promo' => $data]);
    }
    public function peluang_detail($slug)
    {
        $data = $this->peluang_model->get_detail_peluang($slug);
        return view('peluang/detail')->with(['peluang' => $data]);
    }
    public function member_detail($slug)
    {
        $data = $this->member_model->get_detail_member($slug);
        return view('member/detail')->with(['member' => $data]);
    }
    public function blog_detail($slug)
    {
        $data = $this->blog_model->get_detail_blog($slug);
        return view('blog/detail')->with(['blog' => $data]);
    }
}
