<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\dashboard_model;

class authorization extends api_controller
{
    function __construct()
    {
        $this->dash_model = new dashboard_model;
    }
    public function check_status()
    {
        return response(@session()->get('sess_ppob')['id'] ? true : true);
    }
}
