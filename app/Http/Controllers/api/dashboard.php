<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\dashboard_model;

class dashboard extends api_controller
{
    function __construct()
    {
        $this->dash_model = new dashboard_model;
    }
    public function get_banners()
    {
        $data = json_decode($this->dash_model->get_banners())->data;
        $response = [
            'banners' => $data,
            'banners2' => array_reverse($data)
        ];
        return response()->json($response, 200);
    }
}
