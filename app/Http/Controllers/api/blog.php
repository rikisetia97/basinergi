<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\blog_model;
use Illuminate\Http\Request;

class blog extends api_controller
{
    function __construct()
    {
        $this->blog_model = new blog_model;
    }
    public function get_blog(Request $request)
    {
        $search = $request->get('search');
        $category = $request->get('category');


        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->blog_model->get_blog($page, $sort, $limit, $search, $category);
        return response()->json($data, 200);
    }

    public function get_all_kategori()
    {
        $data = $this->blog_model->get_all_kategori();
        return response()->json($data, 200);
    }
}
