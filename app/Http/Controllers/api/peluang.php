<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\peluang_model;
use Illuminate\Http\Request;

class peluang extends api_controller
{
    function __construct()
    {
        $this->peluang_model = new peluang_model;
    }
    public function get_peluang(Request $request)
    {
        $search = $request->get('search');
        $category = $request->get('category');


        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->peluang_model->get_peluang($page, $sort, $limit, $search, $category);
        return response()->json($data, 200);
    }
}
