<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\promo_model;
use Illuminate\Http\Request;

class promo extends api_controller
{
    function __construct()
    {
        $this->promo_model = new promo_model;
    }
    public function get_promo(Request $request)
    {
        $search = $request->get('search');
        $category = $request->get('category');


        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->promo_model->get_promo($page, $sort, $limit, $search, $category);
        return response()->json($data, 200);
    }
    public function get_promo_terbaru(Request $request)
    {
        $search = $request->get('search');
        $category = $request->get('category');


        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->promo_model->get_promo_terbaru($page, $sort, $limit, $search, $category);
        return response()->json($data, 200);
    }
}
