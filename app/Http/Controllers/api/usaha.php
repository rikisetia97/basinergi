<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\usaha_model;
use Illuminate\Http\Request;

class usaha extends api_controller
{
    function __construct()
    {
        $this->usaha_model = new usaha_model;
    }
    public function get_usaha(Request $request)
    {
        $search = $request->get('search');
        $category = $request->get('category');


        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->usaha_model->get_usaha($page, $sort, $limit, $search, $category);
        return response()->json($data, 200);
    }
    public function get_usaha_terbaru(Request $request)
    {
        $search = $request->get('search');
        $category = $request->get('category');


        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->usaha_model->get_usaha_terbaru($page, $sort, $limit, $search, $category);
        return response()->json($data, 200);
    }

    public function get_all_kategori(Request $request)
    {
        $search = $request->get('search');
        $data = $this->usaha_model->get_all_kategori($search);
        return response()->json($data, 200);
    }

    public function get_address(Request $request)
    {
        $address = $request->only('term')['term']['term'];
        $data = $this->usaha_model->get_address($address);
        return response()->json($data, 200);
    }
    public function get_kategori()
    {
        $data = $this->usaha_model->get_kategori();
        return response()->json($data, 200);
    }
}
