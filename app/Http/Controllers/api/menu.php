<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\menu_model;
use Illuminate\Http\Request;

class menu extends api_controller
{
    function __construct()
    {
        $this->menu_model = new menu_model;
    }
    public function update_profile(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        $input = $request->only('name', 'email', 'phone_number', 'address', 'class_year');
        $id = $request->only('id')['id'];
        if ($request->only('file')['file'] != 'undefined') {
            $path = public_path('uploads/images');
            $fileName = time() . '.' . $request->file->extension();
            $request->file->move($path, $fileName);

            $input['image'] =  url('uploads/images/' . $fileName);
        }
        $data = $this->menu_model->update_profile($id, $input);
        return response()->json($data, 200);
    }
    public static function quickRandom($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
    public function add_usaha(Request $request)
    {
        $input = $request->only('business_name', 'location', 'id_category', 'business_post', 'custom_button_1', 'custom_button_2');
        // Upload Logo
        if ($request->only('logo_img')['logo_img'] != 'undefined') {
            $path = public_path('uploads/images/logo');
            $fileName = $this->quickRandom() . '' . time() . '.' . $request->logo_img->extension();
            $request->logo_img->move($path, $fileName);

            $input['logo'] =  url('uploads/images/logo/' . $fileName);
        }
        // Upload Cover
        if ($request->only('cover_img')['cover_img'] != 'undefined') {
            $path = public_path('uploads/images/cover');
            $fileName = $this->quickRandom() . '' . time() . '.' . $request->cover_img->extension();
            $request->cover_img->move($path, $fileName);

            $input['image'] =  url('uploads/images/cover/' . $fileName);
        }
        // Upload Gallery
        $gallery = '';
        if (@$request->only('additional_post')) {
            # code...
            foreach ($request->only('additional_post')['additional_post'] as $additional_post) {
                if ($additional_post != 'undefined') {
                    $path = public_path('uploads/images/gallery');
                    $fileName = $this->quickRandom() . '' . time() . '.' . $additional_post->extension();
                    $additional_post->move($path, $fileName);
                    $gallery .=  ',' . url('uploads/images/gallery/' . $fileName);
                }
            }
            $input['additional_post'] = substr($gallery, 1);
        }
        $input['id_user'] = session()->get('sess_user')['id'];
        $input['slug'] = $this->generate_slug($input['business_name']);
        $data = $this->menu_model->add_usaha($input);
        return response()->json($data, 200);
    }
    public function generate_slug($text, $divider = '-')
    {
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, $divider);
        $text = preg_replace('~-+~', $divider, $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
    public function add_promo(Request $request)
    {
        $input = $request->only('promotion_name', 'id_business', 'expired_time', 'promotion_post', 'custom_button_1');
        // Upload Cover
        if ($request->only('cover_img')['cover_img'] != 'undefined') {
            $path = public_path('uploads/images/cover');
            $fileName = $this->quickRandom() . '' . time() . '.' . $request->cover_img->extension();
            $request->cover_img->move($path, $fileName);

            $input['image'] =  url('uploads/images/cover/' . $fileName);
        }
        $input['slug'] = $this->generate_slug($input['promotion_name']);
        $data = $this->menu_model->add_promo($input);
        return response()->json($data, 200);
    }
    public function add_peluang(Request $request)
    {
        $input = $request->only('peluang_name', 'id_business', 'peluang_post', 'custom_button_1');
        // Upload Cover
        if ($request->only('cover_img')['cover_img'] != 'undefined') {
            $path = public_path('uploads/images/cover');
            $fileName = $this->quickRandom() . '' . time() . '.' . $request->cover_img->extension();
            $request->cover_img->move($path, $fileName);

            $input['image'] =  url('uploads/images/cover/' . $fileName);
        }
        $input['slug'] = $this->generate_slug($input['peluang_name']);
        $data = $this->menu_model->add_peluang($input);
        return response()->json($data, 200);
    }

    public function get_usaha_user()
    {
        $data = $this->menu_model->get_usaha_user(session()->get('sess_user')['id']);
        return response()->json($data, 200);
    }

    // 
    public function update_usaha(Request $request)
    {
        $input = $request->only('id', 'business_name', 'location', 'id_category', 'business_post', 'custom_button_1', 'custom_button_2', 'additional_post_preloaded');
        // Upload Logo
        if (count($request->only('logo_img')) > 0) {
            if (@$request->only('logo_img')['logo_img'] != 'undefined') {
                $path = public_path('uploads/images/logo');
                $fileName = $this->quickRandom() . '' . time() . '.' . $request->logo_img->extension();
                $request->logo_img->move($path, $fileName);

                $input['logo'] =  url('uploads/images/logo/' . $fileName);
            }
        }
        // Upload Cover
        if (count($request->only('cover_img')) > 0) {
            if (@$request->only('cover_img')['cover_img'] != 'undefined') {
                $path = public_path('uploads/images/cover');
                $fileName = $this->quickRandom() . '' . time() . '.' . $request->cover_img->extension();
                $request->cover_img->move($path, $fileName);

                $input['image'] =  url('uploads/images/cover/' . $fileName);
            }
        }
        // Upload Gallery
        $gallery = '';
        if (count($request->only('additional_post')) > 0) {
            foreach (@$request->only('additional_post')['additional_post'] as $additional_post) {
                if ($additional_post != 'undefined') {
                    $path = public_path('uploads/images/gallery');
                    $fileName = $this->quickRandom() . '' . time() . '.' . $additional_post->extension();
                    $additional_post->move($path, $fileName);
                    $gallery .=  ',' . url('uploads/images/gallery/' . $fileName);
                }
            }
        }
        $input['additional_post'] = $input['additional_post_preloaded'] . '' . (@$input['additional_post_preloaded'] ? $gallery : substr($gallery, 1));
        $input['id_user'] = session()->get('sess_user')['id'];
        $input['slug'] = $this->generate_slug($input['business_name']);
        unset($input['additional_post_preloaded']);
        $data = $this->menu_model->update_usaha($input);
        return response()->json($data, 200);
    }

    public function delete_usaha(Request $request)
    {
        $input = $request->only('id');
        $data = $this->menu_model->delete_usaha($input['id']);
        return response()->json($data, 200);
    }
}
