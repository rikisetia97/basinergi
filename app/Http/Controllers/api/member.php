<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\core\api_controller;
use App\Models\api\member_model;
use Illuminate\Http\Request;

class member extends api_controller
{
    function __construct()
    {
        $this->member_model = new member_model;
    }
    public function get_member(Request $request)
    {
        $search = $request->get('search');
        $page = $request->get('page');
        $sort = $request->get('sort');
        $limit = $request->get('limit');
        $data = $this->member_model->get_member($page, $sort, $limit, $search);
        return response()->json($data, 200);
    }
}
