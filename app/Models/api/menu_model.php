<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class menu_model extends Model
{
    public function update_profile($id, $data)
    {
        $update = [];
        foreach ($data as $key => $value) {
            if (!($value == null || $value == 'null' || strpos($value, 'null'))) {
                $update[$key] = $value;
            }
        };
        $update['is_active'] = 1;
        $affected = DB::table('tm_user')->where('id', $id)->update($update);
        $newSession = session()->get('sess_user');
        foreach ($update as $key => $value) {
            $newSession[$key] = $value;
        }
        session()->put('sess_user', $newSession);
        return $affected;
    }

    public function get_usaha($id)
    {
        $query = DB::table('tm_business as b')
            ->select('b.id', 'b.business_name', 'b.location', 'b.image', 'b.created_at', 'b.slug', 'b.logo')
            ->addSelect('c.category_name', 'c.icon')
            ->where('b.id_user', $id)
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->get();
        return $query;
    }
    public function get_usaha_detail($id_user, $id)
    {
        $query = DB::table('tm_business as b')
            ->where('b.id_user', $id_user)
            ->where('b.id', $id)
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->first();
        $query->id = $id;
        return $query;
    }

    public function add_usaha($input)
    {
        $affected = DB::table('tm_business')->insert($input);
        return $affected;
    }
    public function update_usaha($input)
    {
        $affected = DB::table('tm_business')->where('id', $input['id'])->update($input);
        return $affected;
    }
    public function delete_usaha($id)
    {
        $affected = DB::table('tm_business')->where('id', $id)->delete();
        return $affected;
    }

    public function add_promo($input)
    {
        $affected = DB::table('tm_promotion')->insert($input);
        return $affected;
    }

    public function get_promo_detail($id_user, $id)
    {
        $query = DB::table('tm_promotion as p')
            ->where('b.id_user', $id_user)
            ->where('p.id', $id)
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->first();
        $query->id = $id;
        return $query;
    }


    public function add_peluang($input)
    {
        $affected = DB::table('tm_peluang')->insert($input);
        return $affected;
    }

    public function get_promo($id)
    {
        $query = DB::table('tm_promotion as p')
            ->select('p.id', 'p.promotion_name', 'p.image', 'p.expired_time', 'p.slug', 'p.promotion_post')
            ->where('b.id_user', $id)
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->addSelect('b.business_name')
            ->get();
        return $query;
    }
    public function get_usaha_user($id)
    {
        $query = DB::table('tm_business as b')
            ->select('b.id', 'b.business_name', 'b.location', 'b.image', 'b.created_at', 'b.slug', 'b.logo')
            ->where('b.id_user', $id)
            ->get();
        return $query;
    }

    public function get_peluang($id)
    {
        $query = DB::table('tm_peluang as p')
            ->select('p.id', 'p.peluang_name', 'p.image', 'p.slug')
            ->addSelect('b.business_name', 'b.location')
            ->where('b.id_user', $id)
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->get();
        return $query;
    }
}
