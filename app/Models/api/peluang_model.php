<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class peluang_model extends Model
{

    public function get_detail_peluang($slug)
    {
        $query = DB::table('tm_peluang as p')
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->select('b.id', 'b.business_name', 'c.category_name', 'b.location', 'b.logo', 'b.created_at', 'p.peluang_name', 'p.peluang_post',  'p.image', 'p.custom_button_1', 'p.created_at', 'b.slug as business_slug')
            ->where('p.slug', $slug)
            ->first();
        return $query;
    }

    public function get_peluang($page, $sort, $limit, $search, $category)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_peluang as p')
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->select('p.id', 'p.peluang_name', 'p.image', 'p.slug')
            ->where('p.status', 1)
            ->addSelect('b.business_name')
            ->orderBy('p.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('p.peluang_name', $sort);
        }
        if (@$search) {
            $query->where('p.peluang_name', 'like', '%' . $search . '%');
            // $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        if (@$category) {
            $query->where('b.id_category', $category);
        }
        // $query->limit($limit);
        // $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_peluang')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }
}
