<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class promo_model extends Model
{

    public function get_detail_promo($slug)
    {
        $query = DB::table('tm_promotion as p')
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->select('b.id', 'b.business_name', 'c.category_name', 'b.location', 'b.logo', 'b.created_at', 'p.promotion_name', 'p.promotion_post', 'p.image', 'p.expired_time', 'p.custom_button_1', 'b.slug as business_slug')
            ->where('p.slug', $slug)
            ->first();
        return $query;
    }

    public function get_promo($page, $sort, $limit, $search, $category)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_promotion as p')
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->select('p.id', 'p.slug', 'p.promotion_name', 'p.image', 'p.promotion_post', 'p.expired_time')
            ->addSelect('b.business_name')
            ->where('p.status', 1)
            ->orderBy('b.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('p.promotion_name', $sort);
        }
        if (@$search) {
            $query->where('p.promotion_name', 'like', '%' . $search . '%');
            // $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        if (@$category) {
            $query->where('b.id_category', $category);
        }
        // $query->limit($limit);
        // $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_promotion')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }
    public function get_promo_terbaru($page, $sort, $limit, $search, $category)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_promotion as p')
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->select('p.id', 'p.slug', 'p.promotion_name', 'p.image', 'p.promotion_post')
            ->addSelect('b.business_name')
            ->where('p.status', 1)
            ->limit(5)
            ->orderBy('b.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('p.promotion_name', $sort);
        }
        if (@$search) {
            $query->where('p.promotion_name', 'like', '%' . $search . '%');
            // $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        if (@$category) {
            $query->where('b.id_category', $category);
        }
        $query->limit($limit);
        $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_promotion')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }
}
