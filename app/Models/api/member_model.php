<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class member_model extends Model
{

    public function get_detail_member($slug)
    {
        $user = DB::table('tm_user as u')
            ->select('u.id', 'u.name', 'u.email', 'u.image as user_image', 'u.address')
            ->where('u.slug', $slug)
            ->first();
        $business = DB::table('tm_business as b')
            ->select('b.business_name', 'b.location', 'b.image', 'b.created_at', 'b.slug', 'b.logo')
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->addSelect('c.category_name', 'c.icon')
            ->where('b.id_user', $user->id)->get();
        $return = [
            'user' => $user,
            'business' => $business,
        ];
        return $return;
    }


    public function get_member($page, $sort, $limit, $search)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_user as u')
            ->select('u.slug', 'u.name', 'u.address', 'u.image', 'u.created_at', 'u.class_year')
            ->where('u.show_profile', 1)
            ->orderBy('u.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('u.name', $sort);
        }
        if (@$search) {
            $query->where('u.name', 'like', '%' . $search . '%');
            // $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        // $query->limit($limit);
        // $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_user')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }
}
