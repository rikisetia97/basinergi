<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class blog_model extends Model
{

    public function get_detail_blog($slug)
    {
        $query = DB::table('tm_blog as b')
            ->join('tm_blog_category as bc', 'bc.id', '=', 'b.id_blog_category')
            ->select('b.title', 'b.post', 'b.image', 'b.created_at', 'bc.category_name', 'bc.icon')
            ->where('b.slug', $slug)
            ->first();
        return $query;
    }

    public function get_blog($page, $sort, $limit, $search, $category)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_blog as b')
            ->join('tm_blog_category as bc', 'bc.id', '=', 'b.id_blog_category')
            ->select('b.id', 'b.title', 'b.image', 'b.created_at', 'b.slug')
            ->addSelect('bc.category_name', 'bc.icon')
            ->where('b.status', 1)
            ->orderBy('b.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('b.title', $sort);
        }
        if (@$search) {
            $query->where('b.title', 'like', '%' . $search . '%');
            // $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        if (@$category) {
            $query->where('b.id_blog_category', $category);
        }
        // $query->limit($limit);
        // $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_blog')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }
    public function get_all_kategori()
    {
        $query = DB::table('tm_blog_category')->get();
        return $query;
    }
}
