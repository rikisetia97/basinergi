<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class usaha_model extends Model
{

    public function get_detail_usaha($slug)
    {
        $query = DB::table('tm_business as b')
            ->join('tm_user as u', 'u.id', '=', 'b.id_user')
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->select('b.id', 'b.business_name', 'b.business_post', 'b.additional_post', 'b.custom_button_1', 'b.custom_button_2', 'b.location', 'b.image', 'b.created_at', 'u.name', 'u.phone_number', 'u.class_year', 'c.category_name', 'c.icon', 'u.image as image_user', 'u.slug as user_slug', 'u.address')
            ->where('b.slug', $slug)
            ->first();
        return $query;
    }

    public function get_usaha($page, $sort, $limit, $search, $category)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_business as b')
            ->join('tm_user as u', 'u.id', '=', 'b.id_user')
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->select('b.id', 'b.business_name', 'b.location', 'b.image', 'b.created_at', 'b.slug', 'b.logo')
            ->where('b.status', 1)
            ->addSelect('u.name')
            ->addSelect('c.category_name', 'c.icon');
        // ->orderBy('b.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('b.business_name', $sort);
        }
        if (@$search) {
            $query->where('b.business_name', 'like', '%' . $search . '%');
            $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        if (@$category) {
            $query->where('b.id_category', $category);
        }
        // $query->limit($limit);
        // $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_business')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }
    public function get_usaha_terbaru($page, $sort, $limit, $search, $category)
    {
        $page = $page ? $page : 1;
        $limit = $limit ? $limit : 10;
        $query = DB::table('tm_business as b')
            ->join('tm_user as u', 'u.id', '=', 'b.id_user')
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->select('b.id', 'b.business_name', 'b.location', 'b.image', 'b.created_at', 'b.slug')
            ->addSelect('u.name')
            ->addSelect('c.category_name', 'c.icon')
            ->where('b.status', 1)
            ->limit(5)
            ->orderBy('b.created_at', 'desc');
        if (@$sort) {
            $query->orderBy('business_name', $sort);
        }
        if (@$search) {
            $query->where('b.business_name', 'like', '%' . $search . '%');
            $query->orWhere('b.location', 'like', '%' . $search . '%');
        }
        if (@$category) {
            $query->where('b.id_category', $category);
        }
        $query->limit($limit);
        $query->offset(($page - 1) * $limit);
        $user = $query->get();

        $total_pages = DB::table('tm_business')->count();
        $total_page = $total_pages > 0 ? ceil($total_pages / $limit) : 1;
        return ['data' => $user, 'current_page' => $page, 'total_page' => $total_page, 'total_rows' => $total_pages];
    }

    public function get_all_kategori($search = '')
    {
        $query = DB::table('tm_category')
            ->orderBy('total_used', 'desc');
        if (@$search) {
            $query->where('category_name', 'like', '%' . $search . '%')->get();
        }
        $result = $query->get();

        return $result;
    }

    public function get_address($villages)
    {
        $query = DB::table('regencies')
            ->select([
                'id',
                'name as address',
            ])
            ->limit(10)
            ->where('name', 'like', "%$villages%")->get();
        return $query;
    }

    public function get_kategori()
    {
        $query = DB::table('tm_category')->get();
        return $query;
    }
}
