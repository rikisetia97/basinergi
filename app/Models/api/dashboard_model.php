<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class dashboard_model extends Model
{

    public function get_banners()
    {
        $query = DB::table('tm_banners')->where('status', 1)->get();
        return $query;
    }

    // Menu Home

    public function get_all_usaha()
    {
        $query = DB::table('tm_business as b')
            ->select('b.id', 'b.business_name', 'c.category_name', 'c.icon')
            ->where('b.status', 1)
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->limit(5)
            ->get();
        return $query;
    }

    public function get_usaha_baru()
    {
        $query = DB::table('tm_business as b')
            ->select('b.id', 'b.business_name', 'b.image', 'b.location', 'c.category_name', 'b.slug', 'c.icon')
            ->orderBy('b.created_at')
            ->where('b.status', 1)
            ->join('tm_category as c', 'c.id', '=', 'b.id_category')
            ->limit(5)
            ->get();
        return $query;
    }
    public function get_promo_baru()
    {
        $query = DB::table('tm_promotion as p')
            ->select('p.id', 'p.promotion_name', 'p.slug', 'p.image', 'p.promotion_post', 'p.expired_time')
            ->addSelect('b.business_name')
            ->where('p.status', 1)
            ->orderBy('p.created_at')
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->limit(5)
            ->get();
        return $query;
    }
    public function get_peluang_baru()
    {
        $query = DB::table('tm_peluang as p')
            ->select('p.id', 'p.peluang_name', 'p.image', 'p.slug')
            ->addSelect('b.business_name', 'b.location')
            ->orderBy('p.created_at')
            ->where('p.status', 1)
            ->join('tm_business as b', 'b.id', '=', 'p.id_business')
            ->limit(5)
            ->get();
        return $query;
    }
    public function get_blog_baru()
    {
        $query = DB::table('tm_blog as p')
            ->select('p.id', 'p.title', 'p.image', 'p.created_at', 'p.slug')
            ->addSelect('b.category_name', 'b.icon')
            ->orderBy('p.created_at')
            ->where('p.status', 1)
            ->join('tm_blog_category as b', 'b.id', '=', 'p.id_blog_category')
            ->limit(5)
            ->get();
        return $query;
    }
}
