<?php

namespace App\Models\api;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class auth_model extends Model
{
    public function send_login($user)
    {
        $query = DB::table('tm_user')->where('email', $user['email'])->first();
        if (!$query) {
            $data = [
                'name' => $user['name'],
                'email' => $user['email'],
                'image' => $user['image'],
                'show_profile' => 0,
                'slug' => str_replace(' ', '-', strtolower($user['name'])),
            ];
            DB::table('tm_user')->insert($data);

            $query = DB::table('tm_user')->where('email', $user['email'])->first();
        }
        return $query;
    }
}
