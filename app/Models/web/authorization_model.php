<?php

namespace App\Models\web;

use App\Models\core\api_model;

class authorization_model extends api_model
{

    public function send_login($data)
    {
        $phone_number = $data['phone_number'];
        $i = $phone_number[0] . $phone_number[1];
        if ($i == '08') {
            $phone_number = substr($phone_number, 2);
            $data['phone_number'] = '628' . $phone_number;
        }
        $data = $this->post_request('rest/api/v1/dashboard/auth/login', $data);
        return $data;
    }
}
