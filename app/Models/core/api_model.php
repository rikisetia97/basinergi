<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class api_model extends Model
{
    function __construct()
    {
        parent::__construct();
        $this->client = new Client([
            'base_uri' => 'https://dash.basinergi.com/v1',
            // 'timeout'  => 2.0,
            // 'debug' => true
        ]);
    }
    public function get_request($url, $data = [])
    {
        try {
            $result = $this->client->get($url, [
                'query' => $data
            ]);
            $result = $result->getBody()->getContents();
        } catch (ClientException $e) {
            $result = $e->getResponse()->getBody()->getContents();
        }

        return $result;
    }
    public function post_request($url, $data = [])
    {
        try {
            $result = $this->client->post($url, [
                'form_params' => $data
            ]);
            $result = $result->getBody()->getContents();
        } catch (ClientException $e) {
            $result = $e->getResponse()->getBody()->getContents();
        }

        return $result;
    }
}
