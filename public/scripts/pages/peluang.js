"use strict"
var search, typeList = 'list', sort = 'asc'
$('.search-bar').keyup(function () {
    search = this.value
    get_peluang()
})
get_peluang()
function get_peluang() {
    $(".list_peluang").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/peluang",
        data: {
            search,
            sort
        },
        success: function (obj) {
            $(".list_peluang").html('')
            if (typeList == 'list') {
                obj.data.forEach(item => {
                    $(".list_peluang").append(`
                        <div class="col-12 mb-3">
                        <a href="peluang/`+ item.slug + `">
                        <div class="item bg-theme pb-3 rounded-m shadow-l">
                            <div data-card-height="215" class="card mb-2" style="height: 215px;">
                                <h5 class="card-bottom color-white mb-2 ml-2">`+ item.peluang_name + `</h5>
                                <img src="`+ item.image + `" style="height:215px">
                                <div class="card-overlay bg-gradient opacity-70"></div>
                            </div>
                            <div class="content text-left mb-0">
                    <span class="opacity-60"><i class="fa fa-briefcase"></i> Rendy's Meubel</span>
                    <p class="text-capitalize"><i class="fa fa-map-marker-alt text-danger"></i>  kabupaten batang </p>
                </div>
                
                        </div>
                        </a>
                    </div>
                    `)
                });
            } else {
                obj.data.forEach(item => {
                    $(".list_peluang").append(`
                        <div class="col-6 mb-3">
                        <div class="item bg-theme pb-3 rounded-m shadow-l">
                            <div data-card-height="100" class="card mb-2">
                                <img src="`+ item.image + `" style="height:100px">
                            </div>
                            <div class="content mb-0">
                                <div class="row mb-0">
                                    <div class="col-12 text-left">
                                        <h6>`+ item.peluang_name + `</h6>
                                        <span class="opacity-60">`+ item.business_name + `</span>
                                    </div>
                                    <div class="col-12 text-left ">
                                        <a href="peluang/`+ item.slug + `" class="color-highlight" style="font-size: 12px;">Lihat Selengkapnya <i class="ml-1 fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `)
                });
            }
        }
    });
}
function setList(type) {
    $('.setList').removeClass('bg-highlight')
    $('.setList').addClass('bg-theme')
    $('.setList').removeClass('text-light')
    $('.setList').addClass('text-dark')
    $('.setList_' + type).removeClass('bg-theme')
    $('.setList_' + type).addClass('bg-highlight')
    $('.setList_' + type).removeClass('text-dark')
    $('.setList_' + type).addClass('text-light')
    typeList = type
    get_peluang()
}
function setSort(datasort) {
    sort = datasort
    get_peluang()
}