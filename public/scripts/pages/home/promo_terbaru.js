"use strict"
var search
$('.search-bar').keyup(function () {
    search = this.value
    get_promo()
})
get_promo()
function get_promo() {
    $(".list_promo").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/promo_terbaru",
        data: {
            search
        },
        success: function (obj) {
            $(".list_promo").html('')
            obj.data.forEach(item => {
                console.log(item)
                $(".list_promo").append(`
                <div class="col-12 mb-3">
                <a href="promo/detail/`+ item.id + `">
                    <div class="item bg-theme pb-3 rounded-m shadow-l">
                <div data-card-height="215" class="card mb-2" style="height: 215px;">
                    <h5 class="card-bottom color-white mb-2 ml-2">`+ item.promotion_name + `</h5>
                    <img src="`+ item.image + `" style="height:215px">
                    <div class="card-overlay bg-gradient opacity-70"></div>
                </div>
                <h4 class="mb-1 color-highlight ml-2">Rp. `+ numberWithCommas(item.price_after) + `</h4>
                <span class="opacity-60 text-muted ml-2 mb-0"><del>Rp. `+ numberWithCommas(item.price_before) + `</del> <span class="color-highlight">(Diskon ` + (item.discount * 100) + `%)</span></span>
                    </a>
            </div>
                    `)
            });
        }
    });
}
