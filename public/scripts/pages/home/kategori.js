"use strict"
var search, category
$('.search-bar').keyup(function () {
    search = this.value
    get_kategori()
})
get_kategori()
function get_kategori() {
    $(".list_kategori").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/kategori",
        data: {
            search,
            category
        },
        success: function (obj) {
            $(".list_kategori").html('')
            obj.forEach(item => {
                console.log(item)
                $(".list_kategori").append(`
                <div class="col-3 mb-2 mt-2 ">
                            <a href="../usaha?idc=`+ item.id + `" data-card-height="100" class="card card-style mb-1 mx-0 py-3">
                                    <div class=" text-center">
                                    <i class="`+ item.icon + ` font-15 color-highlight"></i><br>
                                    <span class="text-dark font-weight-bold mb-0" style="font-size: 12px;">  `+ item.category_name + `</span>
                                    </div>
                            </a>
                        </div>
                    `)
            });
        }
    });
}
