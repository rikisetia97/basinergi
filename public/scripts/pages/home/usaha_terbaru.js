"use strict"
var search
$('.search-bar').keyup(function () {
    search = this.value
    get_usaha()
})
get_usaha()
function get_usaha() {
    $(".list_usaha").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/usaha_terbaru",
        data: {
            search
        },
        success: function (obj) {
            $(".list_usaha").html('')
            obj.data.forEach(item => {
                console.log(item)
                $(".list_usaha").append(`
                <div class="col-12 mb-3">
                <div class="item bg-theme pb-3 rounded-m shadow-l">
                            <div data-card-height="215" class="card mb-2" style="height: 215px;">
                                <h5 class="card-bottom color-white mb-2 ml-2">`+ item.business_name + `</h5>
                                <img src="`+ item.image + `" style="height:215px">
                                <div class="card-overlay bg-gradient opacity-70"></div>
                            </div>
                            <div class="content text-left mb-0">
                    <span class="opacity-60"><i class="fa `+ item.icon + `"></i> ` + item.category_name + `</span>
                    <p class="text-capitalize"><i class="fa fa-map-marker-alt text-danger"></i> `+ item.location.toLowerCase() + ` </p>
                </div>
                
                        </div>
                
            </div>
                    `)
            });
        }
    });
}
