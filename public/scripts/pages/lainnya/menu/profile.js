"use strict"

$('.email').focus()
$('.email').focus()
$('.phone_number').focus()
$('.phone_number').focus()
$('.angkatan').focus()
$('.angkatan').focus()
$('.name').focus()
$('.name').focus()
$('.angkatan').select2({ theme: 'classic', placeholder: "Harap pilih angkatan", })

$('.kota').select2({
    theme: "classic",
    placeholder: "Harap pilih kota asal anda",
    minimumInputLength: 3,
    ajax: {
        url: '../../api/v1/address',
        dataType: 'json',
        type: "POST",
        data: function (term) {
            return {
                term: term
            };
        },
        processResults: function (data) {
            console.log(data)
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.address,
                        id: item.address,
                    }
                })
            };
        }

    }
})
$('.kota option:eq(0)').prop('selected', true);
$('.angkatan option:eq(0)').prop('selected', true);

$('.btn-upload').click(function (e) {
    e.preventDefault();
    $("#file:hidden").trigger('click');
})

var loadFile = function (event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('output');
        output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
};

$('.btn-simpan').click(function () {
    $('.btn-simpan').html('<i class="fa fa-spinner fa-spin mr-1"></i> Loading')
    var send_data = {
        id: $('.id').val(),
        name: $('.name').val(),
        email: $('.email').val(),
        phone_number: $('.phone_number').val(),
        address: $('.kota').val(),
        class_year: $('.angkatan').val(),
    }
    var formData = new FormData();
    formData.append('id', send_data.id);
    formData.append('name', send_data.name);
    formData.append('email', send_data.email);
    formData.append('phone_number', send_data.phone_number);
    formData.append('address', send_data.address);
    formData.append('class_year', send_data.class_year);
    formData.append('file', $('#file')[0].files[0]);
    $.ajax({
        url: '../../api/v1/menu/profile',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $('.btn-simpan').html('<i class="fa fa-save mr-1"></i> Simpan')
            $('#menu-confirm').removeClass('menu-active')
            $('#menu-success-1').addClass('menu-active')
        }
    });

})

$('.btn-ok').click(function () {
    location.href = './'
})

