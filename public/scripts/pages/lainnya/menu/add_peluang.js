"use strict"

get_usaha()
function get_usaha() {
    $('.usaha').select2({
        theme: 'classic',
        placeholder: "Sedang memproses data...",
    })
    $.ajax({
        type: "GET",
        url: "../../api/v1/menu/get_usaha",
        success: function (response) {
            $('.usaha').html('<option></option>')
            response.forEach(item => {
                $('.usaha').append('<option value="' + item.id + '">' + item.business_name + '</option>')
            });
            $('.usaha').select2({
                theme: 'classic',
                placeholder: "Harap pilih usaha usaha",
            })
        }
    });
}
var myEditor;

// console.log(CKEDITOR)
DecoupledEditor
    .create(document.querySelector('#editor'))
    .then(editor => {
        const toolbarContainer = document.querySelector('#toolbar-container');

        toolbarContainer.appendChild(editor.ui.view.toolbar.element);
        myEditor = editor;

    })
    .catch(error => {
        console.error(error);
    });

$('.btn-upload-cover').click(function (e) {
    e.preventDefault();
    $("#file-cover:hidden").trigger('click');
})

var loadFileCover = function (event) {
    $('#menu-crop').addClass('menu-active')
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('image-crop');
        output.src = reader.result;
        console.log(output.src)
    };
    reader.readAsDataURL(event.target.files[0]);
    setTimeout(() => {
        var image = document.getElementById('image-crop');
        var cropper = new Cropper(image, {
            dragMode: 'move',
            aspectRatio: 16 / 9,
            autoCropArea: 0.65,
            restore: true,
            guides: true,
            center: true,
            highlight: true,
            cropBoxMovable: true,
            cropBoxResizable: true,
            toggleDragModeOnDblclick: true,
        });
        $('.crop-btn').click(function () {
            var output_crop = document.getElementById('image-cover')
            output_crop.src = cropper.getCroppedCanvas().toDataURL('image/jpeg')
        })
    }, 100);
};
function base64ToBlob(base64, mime) {
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: mime });
}


$('#uploaded_file').imageUploader();
function customize_button(div_button, class_btn = '', icon_btn = '') {
    var button = $('#' + div_button)
    if (class_btn != '') {
        button.attr('class', class_btn)
    }
    if (icon_btn != '') {
        button.html(icon_btn + ' <span class="text_' + div_button + '">')
    }
    console.log(div_button)
    $('.text_' + div_button).html($('.title-' + div_button).val())
    button.attr('href', $('.link-' + div_button).val())
}
$('.tab-7').click(function () {
    var i = 0;
    if ($('.judul_peluang').val()) {
        $('#check_judul').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_judul').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('.usaha').val()) {
        $('#check_usaha').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_usaha').attr('class', 'fa fa-times bg-red1-dark') }
    if (myEditor.getData()) {
        $('#check_deskripsi').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_deskripsi').attr('class', 'fa fa-times bg-red1-dark') }
    if ($("input[name='images[]")[0].files.length > 0) {
        $('#check_gallery').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_gallery').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('#image-cover').attr('src') != '/images/avatars/empty_business.png') {
        $('#check_cover').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_cover').attr('class', 'fa fa-times bg-red1-dark') }
    i += 2
    $('#check_custom_button_1').attr('class', 'fa fa-check bg-green1-dark')
    $('#check_custom_button_2').attr('class', 'fa fa-check bg-green1-dark')
    $('.progress-bar').attr('style', 'width:' + i + '0%')
    $('.progress-bar').html(i + '/7 Complete')

})

$('.btn-simpan-promo').click(function () {
    $('.btn-simpan-promo').html('<i class="fa fa-spinner fa-spin mr-1"></i> Loading')
    var formData = new FormData();
    var image = $('#image-cover').attr('src');
    if (image != '/images/avatars/empty_business.png') {
        var base64ImageContent = image.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        var cover_img = base64ToBlob(base64ImageContent, 'image/png');
        formData.append('cover_img', cover_img);
    }
    formData.append('peluang_name', $('.judul_peluang').val());
    formData.append('id_business', $('.usaha').val());
    formData.append('peluang_post', myEditor.getData());
    formData.append('custom_button_1', $('.div-button-1').html());
    $.ajax({
        url: '../../api/v1/menu/add_peluang',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $('.btn-simpan-promo').html('<i class="fa fa-save mr-1"></i> Simpan')
            $('#menu-confirm').removeClass('menu-active')
            $('#menu-success-1').addClass('menu-active')
        },
        error: function (err) {
            $('.btn-simpan-promo').html('<i class="fa fa-save mr-1"></i> Simpan')
            $('#menu-confirm').removeClass('menu-active')
            $('#menu-failed-1').addClass('menu-active')
        }
    });
})

$('.btn-ok').click(function () {
    location.href = 'peluang'
})