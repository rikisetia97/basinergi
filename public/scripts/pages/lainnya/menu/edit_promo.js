"use strict"
$('.nama_usaha').focus()
$('.alamat').focus()

$('.kota').select2({
    theme: "classic",
    placeholder: "Harap pilih alamat asal anda",
    minimumInputLength: 3,
    ajax: {
        url: '../../../api/v1/address',
        dataType: 'json',
        type: "POST",
        data: function (term) {
            return {
                term: term
            };
        },
        processResults: function (data) {
            console.log(data)
            return {
                results: $.map(data, function (item) {
                    return {
                        text: item.address,
                        id: item.address,
                    }
                })
            };
        }

    }
})
$('.kota option:eq(0)').prop('selected', true);
get_kategori()
function get_kategori() {
    $('.kategori').select2({
        theme: 'classic',
        placeholder: "Sedang memproses data...",
    })
    $.ajax({
        type: "GET",
        url: "../../../api/v1/category",
        success: function (response) {
            // $('.kategori').html('<option></option>')
            response.forEach(item => {
                $('.kategori').append('<option value="' + item.id + '">' + item.category_name + '</option>')
            });
            $('.kategori').select2({
                theme: 'classic',
                placeholder: "Harap pilih kategori usaha",
            })
            $('.kategori option:eq(0)').prop('selected', true);
        }
    });
}
var myEditor;

// console.log(CKEDITOR)
DecoupledEditor
    .create(document.querySelector('#editor'))
    .then(editor => {
        const toolbarContainer = document.querySelector('#toolbar-container');

        toolbarContainer.appendChild(editor.ui.view.toolbar.element);
        myEditor = editor;

    })
    .catch(error => {
        console.error(error);
    });

$('.btn-upload-logo').click(function (e) {
    e.preventDefault();
    $("#file-logo:hidden").trigger('click');
})

var loadFile = function (event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('output-logo');
        output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
};

$('.btn-upload-cover').click(function (e) {
    e.preventDefault();
    $("#file-cover:hidden").trigger('click');
})

var loadFileCover = function (event) {
    $('#menu-crop').addClass('menu-active')
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('image-crop');
        output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
    setTimeout(() => {
        var image = document.getElementById('image-crop');
        var cropper = new Cropper(image, {
            dragMode: 'move',
            aspectRatio: 16 / 9,
            autoCropArea: 0.65,
            restore: true,
            guides: true,
            center: true,
            highlight: true,
            cropBoxMovable: true,
            cropBoxResizable: true,
            toggleDragModeOnDblclick: true,
        });
        console.log(cropper)
        $('.crop-btn').click(function () {
            var output_crop = document.getElementById('image-cover')
            output_crop.src = cropper.getCroppedCanvas().toDataURL('image/jpeg')
        })
    }, 100);
};
function base64ToBlob(base64, mime) {
    mime = mime || '';
    var sliceSize = 1024;
    var byteChars = window.atob(base64);
    var byteArrays = [];

    for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
        var slice = byteChars.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: mime });
}

var pre_preloaded = $('.preload_img').val().split(',')
var preloaded = []
pre_preloaded.forEach(item => {
    preloaded.push({
        src: item
    })
});
setTimeout(() => {
    $('#uploaded_file').imageUploader({
        preloaded,
    });
}, 100);
// $('#uploaded_file').imageUploader();
function customize_button(div_button, class_btn = '', icon_btn = '') {
    var button = $('#' + div_button)
    if (class_btn != '') {
        button.attr('class', class_btn)
    }
    if (icon_btn != '') {
        button.html(icon_btn + ' <span class="text_' + div_button + '">')
    }
    console.log(div_button)
    $('.text_' + div_button).html($('.title-' + div_button).val())
    button.attr('href', $('.link-' + div_button).val())
}
$('.tab-7').click(function () {
    var i = 0;
    if ($('.nama_usaha').val()) {
        $('#check_nama_usaha').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_nama_usaha').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('.alamat').val()) {
        $('#check_alamat').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_alamat').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('.kota').val()) {
        $('#check_kota').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_kota').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('.kategori').val()) {
        $('#check_kategori').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_kategori').attr('class', 'fa fa-times bg-red1-dark') }
    if (myEditor.getData()) {
        $('#check_deskripsi').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_deskripsi').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('.uploaded .uploaded-image img').length > 0) {
        $('#check_gallery').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_gallery').attr('class', 'fa fa-times bg-red1-dark') }
    if ($('#image-cover').attr('src') != '/images/avatars/empty_business.png') {
        $('#check_cover').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_cover').attr('class', 'fa fa-times bg-red1-dark') }
    // $('.uploaded .uploaded-image img').each(function () {
    //     console.log($(this).attr('src'));
    // });
    if ($('#output-logo').attr('src') != '/images/avatars/empty.png') {
        $('#check_logo').attr('class', 'fa fa-check bg-green1-dark')
        i++
    } else { $('#check_logo').attr('class', 'fa fa-times bg-red1-dark') }
    i += 2
    $('#check_custom_button_1').attr('class', 'fa fa-check bg-green1-dark')
    $('#check_custom_button_2').attr('class', 'fa fa-check bg-green1-dark')
    $('.progress-bar').attr('style', 'width:' + i + '0%')
    $('.progress-bar').html(i + '/10 Complete')

})
$('.btn-simpan-usaha').click(function () {
    $('.btn-simpan-usaha').html('<i class="fa fa-spinner fa-spin mr-1"></i> Loading')
    $('#button-1').removeAttr('data-menu')
    $('#button-2').removeAttr('data-menu')
    var formData = new FormData();
    var logo = $('#output-logo').attr('src');
    if (logo.includes('https://') === false) {
        var base64ImageContent = logo.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        var logo_img = base64ToBlob(base64ImageContent, 'image/png');
        formData.append('logo_img', logo_img);
    }
    var image = $('#image-cover').attr('src');
    if (image.includes('https://') === false) {
        var base64ImageContent = image.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        var cover_img = base64ToBlob(base64ImageContent, 'image/png');
        formData.append('cover_img', cover_img);
    }
    var gallery = $("input[name='images[]")[0].files
    if (gallery.length > 0) {
        for (var i = gallery.length - 1; i >= 0; i--) {
            formData.append('additional_post[]', gallery[i]);
        }
    }
    console.log(gallery)
    formData.append('id', $('.id_usaha').val());
    formData.append('business_name', $('.nama_usaha').val());
    formData.append('location', $('.alamat').val() + ',' + $('.kota').val());
    formData.append('id_category', $('.kategori').val());
    formData.append('business_post', myEditor.getData());
    formData.append('custom_button_1', $('.div-button-1').html());
    formData.append('custom_button_2', $('.div-button-2').html());
    var additional_post_preloaded = []
    $('.uploaded .uploaded-image img').each(function () {
        var item_uploaded = $(this).attr('src')
        if (!item_uploaded.includes('blob:')) {
            additional_post_preloaded.push(item_uploaded)
        }
    });
    console.log(additional_post_preloaded)
    formData.append('additional_post_preloaded', additional_post_preloaded);
    $.ajax({
        url: '../../../api/v1/menu/update_usaha',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $('#button-1').attr('data-menu', 'customize-button-1')
            $('#button-2').attr('data-menu', 'customize-button-2')
            $('.btn-simpan-usaha').html('<i class="fa fa-save mr-1"></i> Simpan')
            $('#menu-confirm').removeClass('menu-active')
            $('#menu-success-1').addClass('menu-active')
        },
        error: function (err) {
            $('#button-1').attr('data-menu', 'customize-button-1')
            $('#button-2').attr('data-menu', 'customize-button-2')
            $('.btn-simpan-usaha').html('<i class="fa fa-save mr-1"></i> Simpan')
            $('#menu-confirm').removeClass('menu-active')
            $('#menu-failed-1').addClass('menu-active')
        }
    });
})

$('.btn-ok').click(function () {
    location.href = '../usaha'
})

$('.btn-hapus-usaha').click(function () {
    var formData = new FormData();
    formData.append('id', $('.id_usaha').val());
    $.ajax({
        url: '../../../api/v1/menu/delete_usaha',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $('#menu-confirm-delete').removeClass('menu-active')
            $('#menu-success-1').addClass('menu-active')
        },
        error: function (err) {
            $('#menu-confirm-delete').removeClass('menu-active')
            $('#menu-failed-1').addClass('menu-active')
        }
    });
})