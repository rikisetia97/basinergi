"use strict"
var search, typeList = 'list', sort = 'asc'
$('.search-bar').keyup(function () {
    search = this.value
    get_promo()
})
get_promo()
function get_promo() {
    $(".list_member").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/member",
        data: {
            search,
            sort
        },
        success: function (obj) {
            $(".list_member").html('')
            if (typeList == 'list') {

                obj.data.forEach(item => {
                    $(".list_member").append(`
                    <div class="col-12 mb-3">
                            <a href="member/` + item.slug + `" class="color-highlight">
                            <div class="item bg-theme rounded-m shadow-l">
                            <div class="row p-2 mb-0">
                                <div class="col-5 col-md-3">
                                    <img class="rounded-sm" src="` + item.image + `" style="width: 100%; height:100%">
                                </div>
                                <div class="col-5 col-md-7 text-left mt-2" style="margin-left: -15px;">
                                    <h5 class="text-truncate" style="max-width: 180px;">` + item.name + `</h5>
                                    <span class="opacity-60"><i class="fa fa-graduation-cap mr-1"></i> Angkatan ` + item.class_year + `</span>
                                    <p class="text-capitalize text-truncate"><i class="fa fa-map-marker-alt text-danger"></i> `+ (item.address ? item.address.toLowerCase() : 'Not Set') + ` </p>
                                </div>
                                <div class="col-2 text-right">
                                    <i class="fa fa-angle-right fa-2x mt-4"></i>
                                </div>
                            </div>
                        </div>
                            </a>
                        </div>
                    `)
                });
            } else {
                // obj.data.forEach(item => {
                //     $(".list_member").append(`
                //     <div class="col-6 mb-3">
                //     <a href="promo/`+ item.slug + `">
                //     <div class="item bg-theme pb-3 rounded-m shadow-l">
                //     <div data-card-height="80" class="card mb-2">
                //     <img src="`+ item.image + `" style="height:100px">
                //     </div>
                //     <div class="d-flex px-3">
                //     <div>
                //     <h6>`+ item.promotion_name + `</h6>
                //     <h5 class="mb-1 color-highlight">Rp. `+ numberWithCommas(item.price_after) + `</h5>
                //     <span class="opacity-60 text-muted"><del>Rp. `+ numberWithCommas(item.price_before) + `</del> <span class="color-highlight">(` + (item.discount * 100) + `%)</span></span>

                //     </div>
                //     </div>
                //     </div>
                //     </a>
                //     </div>
                //     `)
                // });
            }
        }
    });
}
function setList(type) {
    $('.setList').removeClass('bg-highlight')
    $('.setList').addClass('bg-theme')
    $('.setList').removeClass('text-light')
    $('.setList').addClass('text-dark')
    $('.setList_' + type).removeClass('bg-theme')
    $('.setList_' + type).addClass('bg-highlight')
    $('.setList_' + type).removeClass('text-dark')
    $('.setList_' + type).addClass('text-light')
    typeList = type
    get_promo()
}
function setSort(datasort) {
    sort = datasort
    get_promo()
}