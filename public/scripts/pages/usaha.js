"use strict"
var search, category, typeList = 'list', sort = 'asc'
$('.search-bar').keyup(function () {
    search = this.value
    get_usaha()
})
get_usaha()
function get_usaha() {
    $(".list_usaha").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "api/v1/usaha",
        data: {
            search,
            category,
            sort
        },
        success: function (obj) {
            $(".list_usaha").html('')
            if (typeList == 'list') {
                obj.data.forEach(item => {
                    $(".list_usaha").append(`
                            <div class="col-12 mb-3">
                            <a href="usaha/` + item.slug + `" class="color-highlight">
                                <div class="item bg-theme rounded-m shadow-l">
                                    <div class="row p-2 mb-0">
                                        <div class="col-5 col-md-3">
                                            <img class="rounded-sm" src="` + item.logo + `" style="width: 100%; height:100%">
                                        </div>
                                        <div class="col-5 col-md-7 text-left mt-2" style="margin-left: -15px;">
                                            <h5 class="text-truncate" style="max-width: 180px;">` + item.business_name + `</h5>
                                            <span class="opacity-60"><i class="fa `+ item.icon + `"></i> ` + item.category_name + `</span>
                                            <p class="text-capitalize text-truncate"><i class="fa fa-map-marker-alt text-danger"></i> `+ item.location.toLowerCase() + ` </p>
                                        </div>
                                        <div class="col-2 text-right">
                                            <i class="fa fa-angle-right fa-2x mt-4"></i>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        `)
                });
            } else {
                obj.data.forEach(item => {
                    $(".list_usaha").append(`
                            <div class="col-6 mb-3">
                                <a href="usaha/` + item.slug + `" class="color-highlight">
                                    <div class="item bg-theme rounded-m shadow-l">
                                        <div class="row p-2 mb-0">
                                            <div class="col-12 col-md-12">
                                                <img class="rounded-sm p-1" src="` + item.logo + `" style="width: 100%; height:100%">
                                            </div>
                                            <div class="col-12 col-md-12 mt-2 text-left">
                                                <h5 class="text-truncate" style="max-width: 180px;">` + item.business_name + `</h5>
                                                <span class="opacity-60"><i class="fa `+ item.icon + `"></i> ` + item.category_name + `</span>
                                                <p class="text-capitalize text-truncate"><i class="fa fa-map-marker-alt text-danger"></i> `+ item.location.toLowerCase() + ` </p>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </a>
                            </div>
                        `)
                });

            }

        }
    });
}
var idc = location.search.split('idc=')[1]
if (idc) {
    setFilter(idc)
}
function setFilter(id) {
    category = id
    $('.cat_header').removeClass('bg-highlight')
    $('.cat_header_' + id).addClass('bg-highlight')
    $('.cat_icon').removeClass('color-highlight')
    $('.cat_icon').removeClass('color-white')
    $('.cat_icon').addClass('color-highlight')
    $('.cat_icon_' + id).removeClass('color-highlight')
    $('.cat_icon_' + id).addClass('color-white')
    $('.cat_name').removeClass('color-white')
    $('.cat_name_' + id).addClass('color-white')
    get_usaha()
}

function setList(type) {
    $('.setList').removeClass('bg-highlight')
    $('.setList').addClass('bg-theme')
    $('.setList').removeClass('text-light')
    $('.setList').addClass('text-dark')
    $('.setList_' + type).removeClass('bg-theme')
    $('.setList_' + type).addClass('bg-highlight')
    $('.setList_' + type).removeClass('text-dark')
    $('.setList_' + type).addClass('text-light')
    typeList = type
    get_usaha()
}

function setSort2(datasort) {
    sort = datasort
    get_usaha()
}