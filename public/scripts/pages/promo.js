"use strict"
var search, typeList = 'list', sort = 'asc'
$('.search-bar').keyup(function () {
    search = this.value
    get_promo()
})
get_promo()
function get_promo() {
    $(".list_promo").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/promo",
        data: {
            search,
            sort
        },
        success: function (obj) {
            $(".list_promo").html('')
            if (typeList == 'list') {

                obj.data.forEach(item => {
                    $(".list_promo").append(`
                    <div class="col-12 mb-3">
                        <a href="promo/`+ item.slug + `">
                        <div class="item bg-theme pb-3 rounded-m shadow-l">
                            <div data-card-height="215" class="card mb-2" style="height: 215px;">
                                <h5 class="card-bottom color-white mb-2 ml-2 text-truncate">`+ item.promotion_name + `</h5>
                                <img src="`+ item.image + `" style="height:215px">
                                <div class="card-overlay bg-gradient opacity-70"></div>
                            </div>
                            <div class="content text-left mt-0 mb-0">
                    <span class="opacity-60"><i class="fa fa-briefcase"></i> `+ item.business_name + `</span>
                    <p class="text-capitalize"><i class="fa fa-clock"></i> Berlaku Sampai Tanggal : `+ item.expired_time + ` </p>
                </div>
                        </div>
                        </a>
                    </div>
                    `)
                });
            } else {
                obj.data.forEach(item => {
                    $(".list_promo").append(`
                    <div class="col-6 mb-3">
                    <a href="promo/`+ item.slug + `">
                    <div class="item bg-theme pb-3 rounded-m shadow-l">
                    <div data-card-height="80" class="card mb-2">
                    <img src="`+ item.image + `" style="height:100px">
                    </div>
                    <div class="d-flex px-3">
                    <div>
                    <h6 class="text-truncate" style="width:120px">`+ item.promotion_name + `</h6>
                            <div style="height: 42px;" style="width:200px">
                                `+ item.promotion_post + `
                            </div>
                    </div>
                    </div>
                    </div>
                    </a>
                    </div>
                    `)
                });
            }
        }
    });
}
function setList(type) {
    $('.setList').removeClass('bg-highlight')
    $('.setList').addClass('bg-theme')
    $('.setList').removeClass('text-light')
    $('.setList').addClass('text-dark')
    $('.setList_' + type).removeClass('bg-theme')
    $('.setList_' + type).addClass('bg-highlight')
    $('.setList_' + type).removeClass('text-dark')
    $('.setList_' + type).addClass('text-light')
    typeList = type
    get_promo()
}
function setSort(datasort) {
    sort = datasort
    get_promo()
}