"use strict"
var search, category
$('.search-bar').keyup(function () {
    search = this.value
    get_blog()
})
get_blog()
function get_blog() {
    $(".list_blog").html(` <div class="col-12"> 
    <div class="item bg-theme rounded-m shadow-l text-center p-3">
    <span><i class="fa fa-spinner fa-spin mr-2"></i> Sedang memuat data</span> </div></div> `)
    $.ajax({
        type: "GET",
        url: "../api/v1/blog",
        data: {
            search, category
        },
        success: function (obj) {
            $(".list_blog").html('')
            obj.data.forEach(item => {
                $(".list_blog").append(`
                        <div class="col-12 mb-3">
                        <a href="blog/`+ item.slug + `" class="color-highlight">
                            <div class="item bg-theme pb-3 rounded-m shadow-l">
                                <div data-card-height="200" class="card mb-2">
                                    <img src="`+ item.image + `" style="height:200px">
                                </div>
                                <div class="content text-left mb-0">
                                    <h5>`+ item.title + `</h5>
                                    <span class="opacity-60"><i class="fa `+ item.icon + `"></i> ` + item.category_name + `</span>
                                    <p><i class="fa fa-clock"></i> `+ item.created_at + ` </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    `)
            });
        }
    });
}

function setFilter(id) {
    category = id
    $('.cat_header').removeClass('bg-highlight')
    $('.cat_header_' + id).addClass('bg-highlight')
    $('.cat_icon').removeClass('color-highlight')
    $('.cat_icon').removeClass('color-white')
    $('.cat_icon').addClass('color-highlight')
    $('.cat_icon_' + id).removeClass('color-highlight')
    $('.cat_icon_' + id).addClass('color-white')
    $('.cat_name').removeClass('color-white')
    $('.cat_name_' + id).addClass('color-white')
    get_blog()
}