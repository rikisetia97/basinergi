<?php

use App\Http\Controllers\web\authorization;
use App\Http\Controllers\web\view_collection;
use App\Http\Controllers\web\menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/auth/google', [authorization::class, 'redirectToProvider']);
Route::get('/auth/callback', [authorization::class, 'handleProviderCallback']);
Route::post('/auth/logout', [authorization::class, 'destroySession']);
Route::get('/', [view_collection::class, 'home']);
Route::get('usaha', [view_collection::class, 'usaha']);
Route::get('blog', [view_collection::class, 'blog']);
Route::get('usaha/{slug}', [view_collection::class, 'usaha_detail']);
Route::get('promo/{slug}', [view_collection::class, 'promo_detail']);
Route::get('peluang/{slug}', [view_collection::class, 'peluang_detail']);
Route::get('member/{slug}', [view_collection::class, 'member_detail']);
Route::get('blog/{slug}', [view_collection::class, 'blog_detail']);

// Langsung viewnya
Route::get('home/kategori', function () {
    return view('home/kategori');
});
Route::get('home/usaha_terbaru', function () {
    return view('home/usaha_terbaru');
});
Route::get('home/promo_terbaru', function () {
    return view('home/promo_terbaru');
});
Route::get('peluang', function () {
    return view('peluang/peluang');
});
Route::get('member', function () {
    return view('member/member');
});
Route::get('promo', function () {
    return view('promo/promo');
});
Route::get('lainnya', function () {
    return view('lainnya/lainnya');
});

Route::get('tentang', function () {
    return view('tentang');
});
// Menu
Route::get('lainnya/menu', function () {
    if (!session()->get('sess_user')) {
        return redirect('lainnya');
    }
    return view('lainnya/menu');
});
Route::get('lainnya/menu/profile', [menu::class, 'profile']);
Route::get('lainnya/menu/usaha', [menu::class, 'get_usaha']);
Route::get('lainnya/menu/edit_usaha/{id}', [menu::class, 'edit_usaha']);
Route::get('lainnya/menu/add_usaha', function () {
    if (!session()->get('sess_user')) {
        return redirect('lainnya');
    }
    return view('lainnya/menu/add_usaha');
});
Route::get('lainnya/menu/promo', [menu::class, 'get_promo']);
Route::get('lainnya/menu/edit_promo/{id}', [menu::class, 'edit_promo']);
Route::get('lainnya/menu/add_promo', function () {
    if (!session()->get('sess_user')) {
        return redirect('lainnya');
    }
    return view('lainnya/menu/add_promo');
});
Route::get('lainnya/menu/peluang', [menu::class, 'get_peluang']);
Route::get('lainnya/menu/add_peluang', function () {
    if (!session()->get('sess_user')) {
        return redirect('lainnya');
    }
    return view('lainnya/menu/add_peluang');
});
// Komponen viewnya
Route::get('components/menu-sidebar', function () {
    return view('components/menu-sidebar');
});
Route::get('components/menu-footer', function () {
    return view('components/menu-footer');
});
Route::get('components/menu-share', function () {
    return view('components/menu-share');
});
Route::get('components/menu-colors', function () {
    return view('components/menu-colors');
});
Route::get('components/menu-share-list', function () {
    return view('components/menu-share-list');
});
