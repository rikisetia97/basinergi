<?php

use App\Http\Controllers\api\authorization;
use App\Http\Controllers\api\dashboard;
use App\Http\Controllers\api\peluang;
use App\Http\Controllers\api\promo;
use App\Http\Controllers\api\usaha;
use App\Http\Controllers\api\blog;
use App\Http\Controllers\api\member;
use App\Http\Controllers\api\menu;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('v1/dashboard/get_banners', [dashboard::class, 'get_banners']);
Route::get('v1/check_status', [authorization::class, 'check_status']);

Route::get('v1/usaha', [usaha::class, 'get_usaha']);
Route::get('v1/usaha_terbaru', [usaha::class, 'get_usaha_terbaru']);
Route::get('v1/promo', [promo::class, 'get_promo']);
Route::get('v1/promo_terbaru', [promo::class, 'get_promo_terbaru']);
Route::get('v1/peluang', [peluang::class, 'get_peluang']);
Route::get('v1/kategori', [usaha::class, 'get_all_kategori']);
Route::get('v1/blog', [blog::class, 'get_blog']);
Route::get('v1/member', [member::class, 'get_member']);

Route::post('v1/menu/profile', [menu::class, 'update_profile']);
Route::post('v1/menu/add_usaha', [menu::class, 'add_usaha']);
Route::post('v1/menu/add_promo', [menu::class, 'add_promo']);
Route::post('v1/menu/add_peluang', [menu::class, 'add_peluang']);
Route::get('v1/menu/get_usaha', [menu::class, 'get_usaha_user']);

Route::post('v1/address', [usaha::class, 'get_address']);
Route::get('v1/category', [usaha::class, 'get_kategori']);

Route::post('v1/menu/update_usaha', [menu::class, 'update_usaha']);
Route::post('v1/menu/delete_usaha', [menu::class, 'delete_usaha']);
