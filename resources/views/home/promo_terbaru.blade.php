@extends('template')
@section('title', 'Promo Terbaru')
@section('header')
@include('header')
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-small">
        <h2><a href="{{url('/')}}"><i class="fa fa-arrow-left"></i></a>Promo Terbaru 2021</h2>
    </div>
    <div class="card header-card" data-card-height="115">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="content">
        <div class="search-box bg-theme rounded-m shadow-s bottom-0">
            <i class="fa fa-search"></i>
            <input type="text" class="border-0 search-bar" placeholder="Cari promo disini" data-search>
        </div>
    </div>
    <div class="content mb-0">
        <div class="row mb-0 list_promo">

        </div>
    </div>

    <div class="content mb-0 text-center">
        <a href="{{url('promo')}}" class="btn btn-sm rounded-l bg-highlight"><i data-feather="chevron-down" data-feather-size="15" class="mt-0"></i> Lihat Semua Promo</a>
    </div>
</div>
@endsection