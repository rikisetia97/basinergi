@extends('template')
@section('title', 'Home')
@section('header')
@include('header')
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')
<div class="page-content">
    <div class="page-title page-title-large mb-0">
        <h2 class="greeting-text"></h2>
    </div>
    <div class="card header-card shape-rounded" data-card-height="210">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="content">
        <div class="search-box bg-theme rounded-m shadow-s bottom-0">
            <i class="fa fa-search"></i>
            <input type="text" class="border-0" placeholder="Cari usaha atau produk disini" data-search>
        </div>
        <div class="search-results disabled-search-list card card-style mx-0 mt-3 px-2">
            <div class="list-group list-custom-large">
                @foreach($all_usaha as $item)
                <a href="#" data-filter-item data-filter-name="{{strtolower($item->business_name.' '.$item->category_name)}}">
                    <i class="fa {{$item->icon}} color-gray2-dark"></i>
                    <span>{{$item->business_name}}</span>
                    <strong>{{$item->category_name}}</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Homepage Slider-->
    <div class="single-slider-boxed text-center owl-no-dots owl-carousel">
        @foreach($banners as $item)
        <a href="{{$item->link}}">
            <div class=" card rounded-s shadow-l" data-card-height="320">
                <?php if ($item->is_caption) { ?>
                    <div class="card-bottom">
                        <h1 class="font-24 font-700">{{$item->title}}</h1>
                        <?php echo $item->description; ?>
                    </div>
                    <div class="card-overlay bg-gradient-fade"></div>
                <?php } ?>
                <div class="card-bg owl-lazy" data-src="{{$item->image}}"></div>
            </div>
        </a>
        @endforeach
    </div>

    <!-- Kategori -->
    <div class="content mt-0">
        <h5 class="float-left font-16 font-600">Kategori Usaha</h5>
        <a class="float-right font-12 color-highlight mt-n1" href="{{url('home/kategori')}}">Lihat Semua <i data-feather="chevron-right" data-feather-size="15" class="mt-0"></i></a>
        <div class="clearfix"></div>
    </div>
    <div class="kategori-list-slider owl-carousel mt-3 mb-n1">
        <a href="{{url('usaha?idc=')}}" data-card-height="80" class="card card-style mb-4 mx-0">
            <div class="card-center text-center">
                <i class="fa fa-th-large font-15 color-highlight"></i><br>
                <span class="text-dark font-weight-bold mb-0" style="font-size: 12px;"> Semua</span>
            </div>
        </a>
        @foreach($kategori as $item)
        <a href="{{url('usaha?idc='.$item->id)}}" data-card-height="80" class="card card-style mb-4 mx-0">
            <div class="card-center text-center">
                <i class="{{$item->icon}} font-15 color-highlight"></i><br>
                <span class="text-dark font-weight-bold mb-0" style="font-size: 12px;"> {{$item->category_name}}</span>
            </div>
        </a>
        @endforeach
    </div>
    <div class="content">
        <h5 class="float-left font-16 font-600">Usaha Paling Baru</h5>
        <a class="float-right font-12 color-highlight mt-n1" href="{{url('home/usaha_terbaru')}}">Lihat Semua <i data-feather="chevron-right" data-feather-size="15" class="mt-0"></i></a>
        <div class="clearfix"></div>
    </div>

    <div class="double-slider owl-carousel owl-no-dots text-center">
        @foreach($usaha_baru as $item)
        <a href="{{url('usaha/'.$item->slug)}}" class="color-highlight">
            <div class="item bg-theme pb-3 rounded-m shadow-l">
                <div data-card-height="200" class="card mb-2">
                    <img src="{{$item->image}}" style="height: 100%;">
                </div>
                <div class="content text-left mb-0">
                    <h4>{{$item->business_name}}</h4>
                    <span class="opacity-60"><i class="fa {{$item->icon}}"></i> {{$item->category_name}}</span>
                    <p class="text-capitalize"><i class="fa fa-map-marker-alt text-danger"></i> {{strtolower($item->location)}} </p>
                </div>
            </div>
        </a>
        @endforeach
    </div>
    <div class="content mt-5">
        <h5 class="float-left font-16 font-600">Promo Terbaru {{date('Y')}}</h5>
        <a class="float-right font-12 color-highlight mt-n1" href="{{url('home/promo_terbaru')}}">Lihat Semua <i data-feather="chevron-right" data-feather-size="15" class="mt-0"></i></a>
        <div class="clearfix"></div>
    </div>

    <div class="double-slider owl-carousel owl-no-dots">
        @foreach($promo_baru as $item)
        <a href="{{url('promo/'.$item->slug)}}" class="color-highlight">
            <div class="item bg-theme pb-3 rounded-m shadow-l">
                <div data-card-height="200" class="card mb-2" style="height: 200px;">
                    <h5 class="card-bottom color-white mb-2 ml-2 text-truncate" style="width:300px">{{$item->promotion_name}}</h5>
                    <img src="{{$item->image}}" style="height: 100%;">
                    <div class="card-overlay bg-gradient opacity-70"></div>
                </div>
                <div class="content text-left mt-0 mb-0">
                    <span class="opacity-60"><i class="fa fa-briefcase"></i> {{$item->business_name}}</span>
                    <p class="text-capitalize"><i class="fa fa-clock"></i> Berlaku Sampai Tanggal : {{$item->expired_time}} </p>
                </div>
            </div>
        </a>
        @endforeach
    </div>
    <div class="content mt-5">
        <h5 class="float-left font-16 font-600">Peluang Usaha atau Bisnis</h5>
        <a class="float-right font-12 color-highlight mt-n1" href="{{url('peluang')}}">Lihat Semua <i data-feather="chevron-right" data-feather-size="15" class="mt-0"></i></a>
        <div class="clearfix"></div>
    </div>

    <div class="double-slider owl-carousel owl-no-dots text-center">
        @foreach($peluang_baru as $item)
        <a href="{{url('peluang/'.$item->slug)}}">
            <div class="item bg-theme pb-3 rounded-m shadow-l">
                <div data-card-height="200" class="card mb-2" style="height: 200px;">
                    <h5 class="card-bottom color-white text-left mb-2 ml-2">{{$item->peluang_name}}</h5>
                    <img src="{{$item->image}}" style="height: 100%;">
                    <div class="card-overlay bg-gradient opacity-70"></div>
                </div>
                <div class="content text-left mt-0 mb-0">
                    <span class="opacity-60"><i class="fa fa-briefcase"></i> {{$item->business_name}}</span>
                    <p class="text-capitalize"><i class="fa fa-map-marker-alt text-danger"></i> {{strtolower($item->location)}} </p>
                </div>

            </div>
        </a>
        @endforeach
    </div>
    <div class="content mt-5">
        <h5 class="float-left font-16 font-600">Artikel & Berita</h5>
        <a class="float-right font-12 color-highlight mt-n1" href="{{url('blog')}}">Lihat Semua <i data-feather="chevron-right" data-feather-size="15" class="mt-0"></i></a>
        <div class="clearfix"></div>
    </div>

    <div class="double-slider owl-carousel owl-no-dots text-center">
        @foreach($blog_baru as $item)
        <a href="{{url('blog/'.$item->slug)}}" class="color-highlight">
            <div class="item bg-theme pb-3 rounded-m shadow-l">
                <div data-card-height="200" class="card mb-2">
                    <img src="{{$item->image}}" style="height: 100%;">
                </div>
                <div class="content text-left mb-0">
                    <h5>{{$item->title}}</h5>
                    <span class="opacity-60"><i class="fa {{$item->icon}}"></i> {{$item->category_name}}</span>
                    <p><i class="fa fa-clock"></i> {{$item->created_at}} </p>
                </div>
            </div>
        </a>
        @endforeach
    </div>

    <div class="content mt-5">
        <a href="#" class="back-to-top-badge bg-highlight shadow-xl"><i class="fa fa-caret-up"></i><span>Back to Top</span></a>
    </div>

</div>
@endsection
@section('script')
<script>
    // Onload with refresh
    $(document).ready(function() {
        onload()
    });
    // Onload no refresh
    function onload() {

    }
</script>
@endsection