<!DOCTYPE HTML>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>@yield('title') | Basinergi</title>
    <link rel="stylesheet" type="text/css" href="/styles/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/styles/style.css?v=2">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900|Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/fonts/css/fontawesome-all.min.css">
    <link rel="manifest" href="/_manifest.json" data-pwa-version="set_in_manifest_and_pwa_js">
    <link rel="apple-touch-icon" sizes="180x180" href="/app/icons/icon-192x192.png">
    <!-- Generics -->
    <link rel="icon" href="{{url('images/favicon/favicon-32.png')}}" sizes="32x32">
    <link rel="icon" href="{{url('images/favicon/favicon-128.png')}}" sizes="128x128">
    <link rel="icon" href="{{url('images/favicon/favicon-192.png')}}" sizes="192x192">

    <!-- Android -->
    <link rel="shortcut icon" href="{{url('images/favicon/favicon-196.png')}}" sizes="196x196">

    <!-- iOS -->
    <link rel="apple-touch-icon" href="{{url('images/favicon/favicon-152.png')}}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{url('images/favicon/favicon-167.png')}}" sizes="167x167">
    <link rel="apple-touch-icon" href="{{url('images/favicon/favicon-180.png')}}" sizes="180x180">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="/styles/custom.css?v=5">
    <script type="text/javascript" src="/scripts/jquery.js"></script>
    <script type="text/javascript" src="/scripts/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/decoupled-document/ckeditor.js"></script>
    <!-- Image Uploader -->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link type="text/css" rel="stylesheet" href="/css/image-uploader.min.css">
    <script type="text/javascript" src="/js/image-uploader.min.js"></script>
    <!-- Image Uploader -->
    <!-- Cropper -->
    <link rel="stylesheet" type="text/css" href="https://fengyuanchen.github.io/cropperjs/css/cropper.css">
    <script src="https://fengyuanchen.github.io/cropperjs/js/cropper.js" crossorigin="anonymous"></script>
    <!-- Cropper -->
    <script type="text/javascript" src="/scripts/custom.js?version=1.0.8"></script>
    <div class="generated-script"></div>
    <div class="generated-pwa"></div>
    @yield('style')
</head>

<body class="theme-light" data-highlight="magenta2">

    <div id="preloader">
        <div class="spinner-border color-highlight" role="status"></div>
    </div>
    <div id="page">

        @yield('header')
        @yield('footer')

        @yield('content')
        <!-- end of page content-->
        <div id="menu-share" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-load="{{url('components/menu-share')}}" data-menu-height="290" data-menu-effect="menu-over">
        </div>
        <div id="menu-highlights" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-load="{{url('components/menu-colors')}}" data-menu-height="280" data-menu-effect="menu-over">
        </div>
        <div id="menu-main" class="menu menu-box-left menu-box-detached rounded-m" data-menu-width="260" data-menu-load="{{url('components/menu-sidebar')}}" data-menu-active="nav-welcome" data-menu-effect="menu-over">
        </div>
        <div id="menu-share-list" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="200" data-menu-effect="menu-over" data-menu-load="{{url('components/menu-share-list')}}">
        </div>

    </div>

    <!-- Be sure this is on your main visiting page, for example, the index.html page-->
    <!-- Install Prompt for Android -->
    <div id="menu-install-pwa-android" class="menu menu-box-bottom menu-box-detached rounded-l" data-menu-height="350" data-menu-effect="menu-parallax">
        <div class="boxed-text-l mt-4">
            <img class="rounded-l mb-3" src="{{ url('app/icons/icon-128x128.png') }}" alt="img" width="90">
            <h4 class="mt-3">Basinergi on your Home Screen</h4>
            <p>
                Install Basinergi on your home screen, and access it just like a regular app. It really is that simple!
            </p>
            <a href="#" class="pwa-install btn btn-s rounded-s shadow-l text-uppercase font-900 bg-highlight mb-2">Add to
                Home Screen</a><br>
            <a href="#" class="pwa-dismiss close-menu color-gray2-light text-uppercase font-900 opacity-60 font-10">Maybe
                later</a>
            <div class="clear"></div>
        </div>
    </div>

    <!-- Install instructions for iOS -->
    <div id="menu-install-pwa-ios" class="menu menu-box-bottom menu-box-detached rounded-l" data-menu-height="320" data-menu-effect="menu-parallax">
        <div class="boxed-text-xl mt-4">
            <img class="rounded-l mb-3" src="{{ url('app/icons/icon-128x128.png') }}" alt="img" width="90">
            <h4 class="mt-3">Basinergi on your Home Screen</h4>
            <p class="mb-0 pb-3">
                Install Basinergi on your home screen, and access it just like a regular app. Open your Safari menu and
                tap "Add to Home Screen".
            </p>
            <div class="clear"></div>
            <a href="#" class="pwa-dismiss close-menu color-highlight font-800 opacity-80 text-center text-uppercase">Maybe
                later</a><br>
            <i class="fa-ios-arrow fa fa-caret-down font-40"></i>
        </div>
    </div>

</body>