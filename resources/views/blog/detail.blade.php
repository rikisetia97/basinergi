@extends('template')
@section('title', $blog->title)
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('peluang')}}" class=" color-white header-title header-subtitle">Informasi Berita</a>
    <a href="{{url('peluang')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-small">
        <h2><a href="{{url('/blog')}}"><i class="fa fa-arrow-left"></i></a>Informasi Berita</h2>
    </div>
    <div class="card header-card" data-card-height="85">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="card preload-img mb-0" data-src="{{$blog->image}}" data-card-height="40vh"> </div>

    <div class="card mt-1 mb-0">
        <div class="d-flex content mb-1">
            <!-- left side of profile -->
            <div class="flex-grow-1 mt-1">
                <h1 class="font-700">{{$blog->title}} </h1>
                <p class="font-13 mb-1">
                    <i class="fa {{$blog->icon}} text-danger mr-1"></i> {{$blog->category_name}}
                </p>
                <p>
                    <i class="fa fa-clock mr-1"></i> {{$blog->created_at}}
                </p>
            </div>
        </div>
        <div class="divider mb-0 mt-0"></div>
        <div class="p-3">
            <button class="btn btn-m btn-block bg-success color-white text-uppercase font-900" data-menu="menu-share"><i class="fa fa-share-alt mr-1"></i> Bagikan</button>
        </div>
        <div class="divider mb-0 mt-0"></div>
        <div class="p-3">
            <!-- Content -->
            <?php echo $blog->post; ?>
            <!-- Content -->
        </div>
    </div>

</div>
@endsection