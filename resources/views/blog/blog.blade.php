@extends('template')
@section('title', 'Artikel & Berita')
@section('header')
@include('header')
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-large mb-0">
        <h2 class="greeting-text blog-text"></h2>
    </div>
    <div class="card header-card" data-card-height="95">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>

    <div class="content">
        <div class="search-box bg-theme rounded-m shadow-s bottom-0">
            <i class="fa fa-search"></i>
            <input type="text" class="border-0 search-bar" placeholder="Cari artikel/berita disini" data-search>
        </div>
    </div>
    <div class="user-list-slider-2 owl-carousel mt-0 mb-n1">
        <a href="#" data-card-height="50" class="card card-style mb-4 mx-0 bg-highlight cat_header cat_header_" onclick="setFilter('')">
            <div class="card-center text-center">
                <p class="mt-0 mb-0 color-white cat_name cat_name_"><i class="fa fa-th-large"></i> Semua</p>
            </div>
        </a>
        @foreach($kategori as $item)
        <a href="#" data-card-height="50" class="card card-style mb-4 mx-0 bg-white cat_header cat_header_{{$item->id}}" onclick="setFilter('{{$item->id}}')">
            <div class="card-center text-center">
                <p class="mt-0 mb-0 cat_name cat_name_{{$item->id}}"><i class="fa {{$item->icon}}"></i> {{$item->category_name}}</p>
            </div>
        </a>
        @endforeach
    </div>

    <div class="content mt-0">
        <div class="row mb-0 list_blog">

        </div>
    </div>

</div>
@endsection