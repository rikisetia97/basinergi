@extends('template')
@section('title', $peluang->peluang_name)
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('peluang')}}" class=" color-white header-title header-subtitle">Informasi Peluang</a>
    <a href="{{url('peluang')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-small">
        <h2><a href="{{url('/peluang')}}"><i class="fa fa-arrow-left"></i></a>Informasi Peluang</h2>
    </div>
    <div class="card header-card" data-card-height="85">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="card preload-img mb-0" data-src="{{$peluang->image}}" data-card-height="35vh"> </div>

    <div class="card mt-1 mb-0">
        <div class="d-flex content mb-1">
            <!-- left side of profile -->
            <div class="flex-grow-1">
                <h4 class="font-700">{{$peluang->peluang_name}} </h4>
                <p class="mb-1">
                    <i class="fa fa-clock mr-1"></i> {{$peluang->created_at}}
                </p>
                <!-- <p class="font-13 text-capitalize">
                    <i class="fa fa-map-marker-alt text-danger mr-1"></i> Lokasi
                </p> -->
            </div>
            <!-- right side of profile. increase image width to increase column size-->
        </div>
        <div class="divider mb-0 mt-0"></div>
        <div class="p-3">
            <button class="btn btn-m btn-block bg-success color-white text-uppercase font-900" data-menu="menu-share"><i class="fa fa-share-alt mr-1"></i> Bagikan</button>
        </div>
        <div class="divider mb-0 mt-0"></div>
        <div class="p-3">
            <h5>Deskripsi Promo</h5>
            <!-- Content -->
            <?php echo $peluang->peluang_post; ?>
        </div>
        <div class="divider mb-3 mt-1"></div>
        <div class="content mt-0">
            <div class="row">
                <div class="col-12">
                    <?php echo str_replace('data-menu="customize-button-1"', '', $peluang->custom_button_1); ?>
                </div>
            </div>
        </div>
        <div class="divider b mb-0 mt-0"></div>
        <div class="d-flex p-4 mb-0">
            <div class="w-50 border-right text-center pr-3">
                <img data-src="{{@$peluang->logo ? $peluang->logo : '../images/avatars/empty.png'}}" width="100" class="preload-img">
                <h6 class="font-14 font-600 mt-2 text-center mb-2">{{$peluang->business_name}}</h6>
                <p class="text-muted mt-n3 font-9 font-400 text-center mb-0 pb-0">Usaha</p>
            </div>
            <div class="w-50 pl-3 pt-0 mt-0">
                <p class="text-muted mt-1 font-12 mb-1"><i class="fa fa-briefcase mr-1"></i> {{$peluang->category_name}}</p>
                <h6>Join : {{date('d M Y',strtotime($peluang->created_at))}}</h6>
                <p class="text-muted mt-1 font-12 mb-1 text-capitalize"><i class="fa fa-map-marker-alt text-danger mr-1"></i> {{strtolower($peluang->location)}}</p>
                <a href="{{url('usaha/'.$peluang->business_slug)}}" class="btn btn-s border-secondary color-secondary text-uppercase">Lihat Usaha <i class="fa fa-arrow-right ml-1"></i></a>
            </div>
        </div>
    </div>

</div>
@endsection