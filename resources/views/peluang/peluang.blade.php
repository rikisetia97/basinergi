@extends('template')
@section('title', 'Peluang Usaha')
@section('header')
@include('header')
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-large mb-0">
        <h2 class="greeting-text peluang-text"></h2>
    </div>
    <div class="card header-card" data-card-height="95">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="content">
        <div class="search-box bg-theme rounded-m shadow-s bottom-0">
            <i class="fa fa-search"></i>
            <input type="text" class="border-0 search-bar" placeholder="Cari peluang usaha disini" data-search>
        </div>
    </div>
    <div class="content">
        <div class="row mb-0">
            <div class="col-6 text-left">
                <a href="javascript:void(0)" class="mr-2 icon icon-xs rounded-xs bg-theme text-dark shadow-l" data-menu="menu-share-list"><i class="fa fa-sort"></i> <span class="ml-0">Sort by</span><i class="fa fa-caret-down "></i></a>
            </div>
            <div class="col-6 text-right">
                <a onclick="setList('list')" href="javascript:void(0)" class="setList setList_list mr-2 icon icon-xs rounded-xs bg-highlight text-light shadow-l"><i class="fa fa-th-list"></i></a>
                <a onclick="setList('square')" href="javascript:void(0)" class="setList setList_square icon icon-xs rounded-xs bg-theme text-dark shadow-l"><i class="fa fa-th-large"></i></a>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row mb-0 list_peluang">

        </div>
    </div>

</div>
@endsection