@extends('template')
@section('title', 'Tentang Basinergi')
@section('footer')
@include('footer')
@endsection
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('peluang')}}" class=" color-white header-title header-subtitle">Tentang Basinergi</a>
    <a href="{{url('peluang')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection
@section('content')
<div class="page-content">
    <div class="header bg-highlight header-demo header-logo-app mb-3">
        <a href="{{url('lainnya')}}" class=" color-white header-title header-subtitle">Tentang Basinergi</a>
        <a href="{{url('lainnya')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="card card-style preload-img" data-src="images/pictures/20.jpg" style="background-image: url(&quot;images/pictures/20.jpg&quot;);">
        <div class="card-body text-center">
            <h1 class="color-white pt-4">Basinergi</h1>
            <p class="color-white mt-n2 mb-3 pb-1">We're always listening to your feedback</p>
            <p class="boxed-text-xl color-white opacity-80 pb-2">
                Do you like Basinergi, but want a specific feature? Please send us Feedback and feature suggestions and we'll consider it for future updates!
            </p>
            <a href="#" class="btn btn-m rounded-sm btn-border btn-center-l border-white color-white font-900 text-uppercase mb-4">Request Feature</a>
        </div>
        <div class="card-overlay bg-highlight opacity-95"></div>
    </div>
</div>
@endsection