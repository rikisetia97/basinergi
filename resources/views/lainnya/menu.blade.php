@extends('template')
@section('title', 'Menu Lainnya')
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('lainnya')}}" class=" color-white header-title header-subtitle">Akun Saya</a>
    <a href="{{url('lainnya')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
</div>
@endsection
@section('content')

<div class="page-content">
    <div class="header bg-highlight header-demo header-logo-app mb-3">
        <a href="{{url('lainnya')}}" class=" color-white header-title header-subtitle">Akun Saya</a>
        <a href="{{url('lainnya')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="menu-logo text-center mt-3 mb-0">
        <a href="#"><img class="rounded-circle bg-highlight" width="100" src="{{ @session()->get('sess_user')['image'] ? session()->get('sess_user')['image'] : '/images/avatars/4m.png' }}"></a>
        <h1 class="pt-3 font-800 font-28 text-uppercase">{{ @session()->get('sess_user')['name'] ? session()->get('sess_user')['name'] : 'Basinergi' }}</h1>
        <p class="font-11 mt-n2">{{ @session()->get('sess_user')['email'] ? session()->get('sess_user')['email'] : '-' }}</p>
    </div>
    <div class="divider-icon divider-margins bg-blue-dark mt-3 mb-3"><i class="fa font-10 color-blue-dark fa-cog"></i></div>
    <div class="card card-style">
        <div class="content mt-0 mb-2">
            <div class="list-group list-custom-large mb-4">
                <a href="{{url('lainnya/menu/profile')}}">
                    <i class="fa font-14 fa-user bg-blue2-dark color-white rounded-sm"></i>
                    <span>Ganti Data Pribadi</span>
                    <strong>Ubah informasi data pribadi anda agar sesuai. </strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <?php if (@session()->get('sess_user')['is_active']) { ?>
                    <a href="{{url('lainnya/menu/usaha')}}">
                        <i class="fa font-14 fa-briefcase bg-red2-dark color-white rounded-sm"></i>
                        <span>Tambahkan Usaha</span>
                        <strong>Daftarkan usaha anda di basinergi.</strong>
                        <i class="fa fa-angle-right mr-2"></i>
                    </a>
                    <a href="{{url('lainnya/menu/promo')}}">
                        <i class="fa font-14 fa-star bg-green2-dark color-white rounded-sm"></i>
                        <span>Tambahkan Promo</span>
                        <strong>Daftarkan promo terkait usaha anda.</strong>
                        <i class="fa fa-angle-right mr-2"></i>
                    </a>
                    <a href="{{url('lainnya/menu/peluang')}}">
                        <i class="fa font-14 fa-shopping-bag bg-brown2-dark color-white rounded-sm"></i>
                        <span>Tambahkan Peluang Usaha</span>
                        <strong>Daftarkan peluang terkait usaha anda.</strong>
                        <i class="fa fa-angle-right mr-2"></i>
                    </a>
                <?php } else { ?>
                    <a href="javascript:void(0)" data-menu="lengkapi-profile">
                        <i class="fa font-14 fa-briefcase bg-red2-dark color-white rounded-sm"></i>
                        <span>Tambahkan Usaha</span>
                        <strong>Daftarkan usaha anda di basinergi.</strong>
                        <i class="fa fa-angle-right mr-2"></i>
                    </a>
                    <a href="javascript:void(0)" data-menu="lengkapi-profile">
                        <i class="fa font-14 fa-star bg-green2-dark color-white rounded-sm"></i>
                        <span>Tambahkan Promo</span>
                        <strong>Daftarkan promo terkait usaha anda.</strong>
                        <i class="fa fa-angle-right mr-2"></i>
                    </a>
                    <a href="javascript:void(0)" data-menu="lengkapi-profile">
                        <i class="fa font-14 fa-shopping-bag bg-brown2-dark color-white rounded-sm"></i>
                        <span>Tambahkan Peluang Usaha</span>
                        <strong>Daftarkan peluang terkait usaha anda.</strong>
                        <i class="fa fa-angle-right mr-2"></i>
                    </a>
                <?php } ?>
            </div>
            <h5>Menu Lainnya</h5>
            <p class="mb-2">
                Fitur tambahan untuk customize tampilan aplikasi.
            </p>
            <div class="list-group list-custom-large mb-0">
                <a href="#" data-toggle-theme class="show-on-theme-light">
                    <i class="fa font-14 fa-moon bg-brown1-dark rounded-sm"></i>
                    <span>Dark Mode</span>
                    <strong>Ubah tampilan menjadi gelap</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="#" data-toggle-theme class="show-on-theme-dark">
                    <i class="fa font-14 fa-lightbulb bg-yellow1-dark rounded-sm"></i>
                    <span>Light Mode</span>
                    <strong>Ubah tampilan menjadi terang</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="#" data-menu="menu-highlights">
                    <i class="fa font-14 fa-brush bg-highlight color-white rounded-sm"></i>
                    <span>Color Scheme</span>
                    <strong>Ubah tema warna sesuai keinginan anda</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>

            </div>

        </div>
    </div>

</div>
<div id="lengkapi-profile" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="270" data-menu-effect="menu-over" style="display: block; height: 305px;">
    <h1 class="text-center mt-4"><i class="fa fa-3x fa-info-circle color-blue1-dark"></i></h1>
    <h1 class="text-center mt-3 text-uppercase font-700">Perhatian</h1>
    <p class="boxed-text-l">
        Harap lengkapi data diri anda terlebih dahulu.
    </p>
    <a href="{{url('lainnya/menu/profile')}}" class="btn-ok btn btn-m btn-center-m button-s shadow-l rounded-s text-uppercase font-900 bg-blue1-light">Lengkapi Data Diri</a>
</div>
@endsection