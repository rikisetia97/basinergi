@extends('template')
@section('title', 'Menu Lainnya')
@section('header')
@include('header')
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')

<div class="page-content">

    <div class="page-title page-title-large mb-0">
        <h2 class="greeting-text lainnya-text"></h2>
    </div>
    <div class="card header-card " data-card-height="210">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>

    <div class="card card-style mt-5">
        <div class="menu-logo text-center mt-5">
            <a href="#"><img class="rounded-circle bg-white" width="100" src="{{ @session()->get('sess_user')['image'] ? session()->get('sess_user')['image'] : '/images/avatars/4m.png' }}"></a>
            <h1 class="pt-3 font-800 font-28 text-uppercase">{{ @session()->get('sess_user')['name'] ? session()->get('sess_user')['name'] : 'Basinergi' }}</h1>
            <p class="font-11 mt-n2">{{ @session()->get('sess_user')['email'] ? session()->get('sess_user')['email'] : 'Selamat datang di basinergi.' }}</p>
        </div>
        <div class="divider bg-blue-dark divider-margins mt-0"></div>
        @if(session()->get('sess_user'))
        <div class="row mb-0">
            <div class="col-6">
                <a href="{{url('lainnya/menu')}}" class="btn btn-3d btn-icon btn-full btn-margins rounded-sm shadow-xl btn-m text-uppercase font-900 border-blue2-dark bg-blue2-light"><i class="fa fa-cogs mr-2"></i>Menu</a>
            </div>
            <div class="col-6">
                <form action="auth/logout" method="post" class="form-logout">
                    {{ csrf_field() }}
                    <a href="javascript:void(0)" onclick="$('.form-logout').submit();" class="btn btn-full  btn-icon btn-3d btn-margins  rounded-sm shadow-xl btn-m text-uppercase font-900 border-red2-dark bg-red2-light"><i class="fa fa-sign-out-alt mr-2"></i> Logout</a>
                </form>
            </div>
        </div>
        @else
        <a href="{{url('auth/google')}}" class="btn btn-full btn-icon btn-3d btn-margins bg-highlight rounded-sm shadow-xl btn-m text-uppercase font-900 border-blue2-dark bg-blue2-light"><i class="fab fa-google mr-2"></i> Login via Google</a>
        @endif
    </div>
    <div class="card card-style">
        <div class="content mt-0 mb-2">
            <div class="list-group list-custom-large mb-0">
                <a href="{{url('/')}}">
                    <i class="fa font-14 fa-home bg-blue2-dark color-white rounded-sm"></i>
                    <span>Beranda</span>
                    <strong>Menu utama aplikasi</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="{{url('usaha')}}">
                    <i class="fa font-14 fa-briefcase bg-red2-dark color-white rounded-sm"></i>
                    <span>Usaha</span>
                    <strong>Daftar usaha yang terdaftar</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="{{url('promo')}}">
                    <i class="fa font-14 fa-star bg-green2-dark color-white rounded-sm"></i>
                    <span>Promo</span>
                    <strong>Daftar promo yang ada.</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="{{url('peluang')}}">
                    <i class="fa font-14 fa-shopping-bag bg-brown2-dark color-white rounded-sm"></i>
                    <span>Peluang</span>
                    <strong>Daftar peluang usaha.</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="{{url('blog')}}">
                    <i class="fa font-14 fa-newspaper bg-info color-white rounded-sm"></i>
                    <span>Artikel/Berita</span>
                    <strong>Daftar artikel/berita di basinergi.</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="{{url('member')}}">
                    <i class="fa font-14 fa-users bg-yellow2-dark color-white rounded-sm"></i>
                    <span>Member</span>
                    <strong>Daftar member terdaftar di basinergi.</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
                <a href="{{url('tentang')}}">
                    <i class="fa font-14 fa-info-circle bg-secondary color-white rounded-sm"></i>
                    <span>Tentang</span>
                    <strong>Tentang aplikasi ini.</strong>
                    <i class="fa fa-angle-right mr-2"></i>
                </a>
            </div>
        </div>
    </div>

</div>
@endsection