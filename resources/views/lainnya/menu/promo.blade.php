@extends('template')
@section('title', 'Daftar Promo Anda')
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('lainnya/menu')}}" class=" color-white header-title header-subtitle">Daftar Promo Anda</a>
    <a href="{{url('lainnya/menu')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
</div>
@endsection
@section('footer')
<div id="footer-bar" class="p-2">
    <a href="{{url('lainnya/menu/add_promo')}}" class="btn btn-3d btn-m btn-full rounded-sm text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light"> Tambah Promo Baru </a>
</div>
@endsection
@section('content')
<div class="page-content">
    <div class="header bg-highlight header-demo header-logo-app mb-3">
        <a href="{{url('lainnya/menu')}}" class=" color-white header-title header-subtitle">Daftar Promo Saya</a>
        <a href="{{url('lainnya/menu')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
    </div>
    <div class="content list_usaha row mb-0 mx-0">
        @if(count($data) == 0)
        <div class="col-12 mb-3" style="bottom:50%;position:absolute;text-align:center">
            <span>Anda belum memiliki promo.</span>
        </div>
        @endif
        @foreach($data as $item)
        <div class="col-12 mb-3">
            <a href="#">
                <!-- <a href="{{url('lainnya/menu/edit_promo/'.$item->id)}}"> -->
                <div class="item bg-theme pb-3 rounded-m shadow-l">
                    <div data-card-height="200" class="card mb-2" style="height: 200px;">
                        <h5 class="card-bottom color-white mb-2 ml-2 text-truncate" style="width:300px">{{$item->promotion_name}}</h5>
                        <img src="{{$item->image}}" style="height:200px">
                        <div class="card-overlay bg-gradient opacity-70"></div>
                    </div>
                    <div class="content text-left mt-0 mb-0">
                        <span class="opacity-60"><i class="fa fa-briefcase"></i> {{$item->business_name}}</span>
                        <p class="text-capitalize"><i class="fa fa-clock"></i> Berlaku Sampai Tanggal : {{$item->expired_time}} </p>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection