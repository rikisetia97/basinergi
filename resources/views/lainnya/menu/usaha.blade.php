@extends('template')
@section('title', 'Daftar Usaha')
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('lainnya/menu')}}" class=" color-white header-title header-subtitle">Daftar Usaha Saya</a>
    <a href="{{url('lainnya/menu')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection
@section('footer')
<div id="footer-bar" class="p-2">
    <a href="{{url('lainnya/menu/add_usaha')}}" class="btn btn-3d btn-m btn-full rounded-sm text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light"> Tambah Usaha Baru </a>
</div>
@endsection
@section('content')

<div class="page-content">
    <div class="header bg-highlight header-demo header-logo-app mb-3">
        <a href="{{url('lainnya/menu')}}" class=" color-white header-title header-subtitle">Daftar Usaha Saya</a>
        <a href="{{url('lainnya/menu')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
    </div>
    <div class="content list_usaha row mb-0 mx-0">
        @if(count($data) == 0)
        <div class="col-12 mb-3" style="bottom:50%;position:absolute;text-align:center">
            <span>Anda belum memiliki usaha.</span>
        </div>
        @endif
        @foreach($data as $item)
        <div class="col-12 mb-3">
            <a href="{{url('lainnya/menu/edit_usaha/'.$item->id)}}" class="color-highlight">
                <div class="item bg-theme rounded-sm shadow-l">
                    <div class="row p-2 mb-0">
                        <div class="col-4 col-md-3">
                            <img class="rounded-sm" src="{{$item->logo}}" style="width: 100%; height:100%">
                        </div>
                        <div class="col-6 col-md-7 text-left" style="margin-left: -15px;">
                            <h5 class="text-truncate" style="max-width: 200px;">{{$item->business_name}}</h5>
                            <span class=" opacity-60"><i class="fa {{$item->icon}} mr-1"></i> {{$item->category_name}}</span>
                            <p class="text-capitalize text-truncate"><i class="fa fa-map-marker-alt text-danger"></i> {{strtolower($item->location)}} </p>
                        </div>
                        <div class="col-2 text-right">
                            <i class="fa fa-angle-right fa-2x mt-4"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection