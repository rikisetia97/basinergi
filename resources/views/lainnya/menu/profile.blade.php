@extends('template')
@section('title', 'Ganti Data Pribadi')
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('lainnya/menu')}}" class=" color-white header-title header-subtitle">Data Pribadi</a>
    <a href="{{url('lainnya/menu')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
</div>
@endsection
@section('content')

<div class="page-content">
    <div class="header bg-highlight header-demo header-logo-app mb-3">
        <a href="{{url('lainnya/menu')}}" class=" color-white header-title header-subtitle">Data Pribadi</a>
        <a href="{{url('lainnya/menu')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    </div>
    <div class="card card-style mb-0">
        <div class="content mb-0">
            <div class="menu-logo text-center mt-3 mb-0">
                <a href="#"><img id="output" class="rounded-circle bg-highlight" width="100" src="{{ @session()->get('sess_user')['image'] ? session()->get('sess_user')['image'] : '/images/avatars/empty.png' }}"></a>
                <p class="font-11 mt-3">
                    <a href="javascript:void(0)" class="btn-upload btn btn-3d btn-icon btn-sm mb-3 rounded-xs text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light"><i class="fa fa-upload mr-1"></i> Upload Foto</a>
                    <input id="file" type="file" onchange="loadFile(event)" style="display: none;" />
                </p>
            </div>
            <input type="hidden" class="id" value="{{session()->get('sess_user')['id']}}">
            <div class="input-style input-style-2 has-icon input-required mt-5">
                <i class="input-icon fa fa-user"></i>
                <span class="color-highlight">Name</span>
                <em>(required)</em>
                <input class="form-control name" type="name" placeholder="" value="{{session()->get('sess_user')['name']}}">
            </div>

            <div class="input-style input-style-2  has-icon input-required">
                <i class="input-icon fa fa-envelope"></i>
                <span class="color-highlight">Email</span>
                <em>(required)</em>
                <input class="form-control email" type="email" placeholder="" value="{{session()->get('sess_user')['email']}}" readonly>
            </div>
            <div class="input-style input-style-2 has-icon input-required">
                <i class="input-icon fa fa-phone"></i>
                <span class="color-highlight">Nomor HP</span>
                <em>(required)</em>
                <input class="form-control phone_number" type="tel" placeholder="" value="{{session()->get('sess_user')['phone_number']}}">
            </div>
            <div class="form-group">
                <select class="form-control kota">

                    <option value="{{session()->get('sess_user')['address']}}">{{session()->get('sess_user')['address']}}</option>
                </select>
            </div>
            <div class="form-group">
                <select class="form-control angkatan">
                    @if(session()->get('sess_user')['class_year'])
                    <option value="{{session()->get('sess_user')['class_year']}}" selected>{{session()->get('sess_user')['class_year']}}</option>
                    @else
                    <option value="" disabled selected>Pilih Angkatan</option>
                    @endif
                    @php
                    for ($i=date('Y'); $i >= 1954; $i--) { @endphp <option value="{{$i}}">{{$i}}</option> @php } @endphp

                </select>
            </div>
            <div class="divider mb-3"></div>
            <a href="#" data-menu="menu-confirm" class="btn btn-3d btn-icon btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s border-red2-dark bg-red2-light"><i class="fa fa-save mr-1"></i> Simpan Data</a>
        </div>
    </div>
</div>
<div id="menu-confirm" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="170" data-menu-effect="menu-over" style="height: 200px;">
    <h2 class="text-center font-700 mt-3 pt-1">Apakah anda yakin?</h2>
    <p class="boxed-text-l">
        Anda akan mengubah data pribadi anda.
    </p>
    <div class="row mr-3 ml-3">
        <div class="col-6">
            <a href="javascript:void(0)" class="btn-simpan btn btn-sm btn-full button-s shadow-l rounded-s text-uppercase font-900 bg-green1-dark"><i class="fa fa-save mr-1"></i> Simpan</a>
        </div>
        <div class="col-6">
            <a href="javascript:void(0)" class="close-menu btn btn-sm btn-full button-s shadow-l rounded-s text-uppercase font-900 bg-red1-dark"><i class="fa fa-times mr-1"></i> Batalkan</a>
        </div>
    </div>
</div>
<div id="menu-success-1" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="270" data-menu-effect="menu-over" style="display: block; height: 305px;">
    <h1 class="text-center mt-4"><i class="fa fa-3x fa-check-circle color-green1-dark"></i></h1>
    <h1 class="text-center mt-3 text-uppercase font-700">Berhasil</h1>
    <p class="boxed-text-l">
        Anda telah berhasil mengubah data diri anda.
    </p>
    <a href="#" class="btn-ok btn btn-m btn-center-m button-s shadow-l rounded-s text-uppercase font-900 bg-green1-light">Oke</a>
</div>
@endsection