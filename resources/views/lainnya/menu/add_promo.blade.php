@extends('template')
@section('title', 'Tambah Promo Baru')
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('lainnya/menu/promo')}}" class=" color-white header-title header-subtitle">Tambah Promo</a>
    <a href="{{url('lainnya/menu/promo')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection

@section('content')

<div class="page-content">
    <div class="header bg-highlight header-demo header-logo-app mb-3">
        <a href="{{url('lainnya/menu/promo')}}" class=" color-white header-title header-subtitle">Tambah Promo</a>
        <a href="{{url('lainnya/menu/promo')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
    </div>
    <div class="card card-style bg-theme pb-0">
        <div class="content">
            <div class="tab-controls tabs-round tab-animated tabs-medium tabs-rounded shadow-xl" data-tab-items="2" data-tab-active="bg-highlight color-white">
                <a href="#" data-tab-active data-tab="tab-5">Langkah 1</a>
                <a href="#" data-tab="tab-6" class="tab-6">Langkah 2</a>
            </div>
            <div class="clearfix mb-3"></div>
            <div class="tab-content" id="tab-5">
                <div class="menu-logo text-center mt-3 mb-5">
                    <a href="#"><img class="bg-highlight" id="image-cover" width="100%" src="/images/avatars/empty_business.png"></a>
                    <p class="font-11 mt-1">
                        <input type="file" id="file-cover" style="display: none;" onchange="loadFileCover(event)">
                        <a href="#" class="btn-upload-cover btn btn-3d btn-icon btn-sm mb-3 rounded-xs text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light"><i class="fa fa-edit"></i> Ubah Cover</a>
                    </p>
                </div>
                <div class="input-style input-style-2 has-icon input-required mt-5">
                    <i class="input-icon fa fa-briefcase"></i>
                    <span class="color-highlight">Judul Promo</span>
                    <input class="form-control judul_promo" type="text" placeholder="">
                </div>
                <select class="form-control usaha" style="width: 100%;">
                    <option value="" disabled selected>Pilih Usaha Anda</option>
                </select>
                <div class="input-style input-style-2 has-icon input-required mt-3">
                    <i class="input-icon fa fa-calendar"></i>
                    <span class="color-highlight">Kadaluarsa</span>
                    <input class="form-control kadaluarsa" type="date">
                </div>
                <div class="divider mb-3"></div>
                <a href="javascript:void(0)" onclick="$('.tab-6').click()" class="btn btn-3d btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light">Lanjut - Langkah 2 <i class="fa fa-arrow-right mr-1"></i></a>
            </div>
            <div class="tab-content" id="tab-6">
                <span><i class="fa fa-info-circle"></i> Tekan tombol untuk ubah warna, text, link & icon.</span>
                <div class="row mb-0">
                    <div class="col-12 div-button-1">
                        <a href="#" id="button-1" class="btn btn-m btn-full bg-success color-white text-uppercase font-900" data-menu="customize-button-1"><span class="text_button-1">Button 1</span></a>
                    </div>
                </div>
                <div class="divider mt-3 mb-3"></div>
                <!-- The toolbar will be rendered in this container. -->
                <div id="toolbar-container"></div>

                <!-- This container will become the editable. -->
                <div id="editor">
                    <p>Edit deskripsi promo disini...<br><br><br><br><br></p>
                </div>

                <div class="divider mt-3 mb-3"></div>
                <a href="javascript:void(0)" class="btn btn-icon btn-3d btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" data-menu="menu-confirm"><i class="fa fa-save mr-1"></i>Simpan Data</a>
            </div>
        </div>
    </div>
</div>
<!-- Tips Menu 1-->
<div id="customize-button-1" class="p-3 menu menu-box-bottom menu-box-detached rounded-s" data-menu-height="500" data-menu-effect="menu-over">
    <h1 class="text-center font-700 mt-2 pt-2">Edit Tampilan Button</h1>
    <p class="boxed-text-xl under-heading">
        Berikut beberapa pilihan button yang dapat dikustomisasikan.
    </p>
    <div class="divider mb-3"></div>
    <span class="font-weight-bold">1. Input title button :</span>
    <div class="input-style input-style-2 mb-2">
        <input class="form-control title-button-1" type="text" placeholder="Masukkan judul button" value="Button 1" onkeyup="customize_button('button-1')">
    </div>
    <span class="font-weight-bold">2. Input link button :</span>
    <div class="input-style input-style-2 mb-2">
        <input class="form-control link-button-1" type="text" placeholder="Masukkan link button" value="#" onkeyup="customize_button('button-1')">
    </div>
    <span class="font-weight-bold">3. Pilih bentuk icon tombol (slide for more):</span>
    <div class="icon-slider owl-carousel mt-2 mb-3">
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fab fa-whatsapp mr-1'></i>`)"><i class="mt-1 fab fa-2x fa-whatsapp"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fab fa-facebook mr-1'></i>`)"><i class="mt-1 fab fa-2x fa-facebook"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fab fa-google mr-1'></i>`)"><i class="mt-1 fab fa-2x fa-google"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fa fa-link mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-link"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fa fa-home mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-home"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fa fa-user mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-user"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-1','',`<i class='fa fa-share mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-share"></i></div>
    </div>
    <span class="font-weight-bold">4. Pilih bentuk/warna tombol (slide for more):</span>
    <div class="button-slider owl-carousel mt-2">
        <a href="#" class="btn btn-m btn-full bg-success color-white text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full bg-success color-white text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full bg-info color-white text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full bg-info color-white text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full bg-danger color-white text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full bg-danger color-white text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full bg-warning color-white text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full bg-warning color-white text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full btn-outline-info text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full btn-outline-info text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full btn-outline-warning text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full btn-outline-warning text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full btn-outline-secondary text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full btn-outline-secondary text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full btn-outline-primary text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full btn-outline-primary text-uppercase font-900')">Button 1</a>
        <a href="#" class="btn btn-m btn-full btn-outline-success text-uppercase font-900" data-menu="customize-button-1" onclick="customize_button('button-1','btn btn-m btn-full btn-outline-success text-uppercase font-900')">Button 1</a>
    </div>
</div>
<div id="customize-button-2" class="p-3 menu menu-box-bottom menu-box-detached rounded-s" data-menu-height="500" data-menu-effect="menu-over">
    <h1 class="text-center font-700 mt-2 pt-2">Edit Tampilan Button</h1>
    <p class="boxed-text-xl under-heading">
        Berikut beberapa pilihan button yang dapat dikustomisasikan.
    </p>
    <div class="divider mb-3"></div>
    <span class="font-weight-bold">1. Input title button :</span>
    <div class="input-style input-style-2 mb-2">
        <input class="form-control title-button-2" type="text" placeholder="Masukkan judul button" value="Button 2" onkeyup="customize_button('button-2')">
    </div>
    <span class="font-weight-bold">2. Input link button :</span>
    <div class="input-style input-style-2 mb-2">
        <input class="form-control link-button-2" type="text" placeholder="Masukkan link button" value="#" onkeyup="customize_button('button-2')">
    </div>
    <span class="font-weight-bold">3. Pilih bentuk icon tombol (slide for more):</span>
    <div class="icon-slider owl-carousel mt-2 mb-3">
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fab fa-whatsapp mr-1'></i>`)"><i class="mt-1 fab fa-2x fa-whatsapp"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fab fa-facebook mr-1'></i>`)"><i class="mt-1 fab fa-2x fa-facebook"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fab fa-google mr-1'></i>`)"><i class="mt-1 fab fa-2x fa-google"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fa fa-link mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-link"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fa fa-home mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-home"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fa fa-user mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-user"></i></div>
        <div class="bg-highlight rounded-sm mr-1 color-white border text-center p-2" style="cursor: pointer;" onclick="customize_button('button-2','',`<i class='fa fa-share mr-1'></i>`)"><i class="mt-1 fa fa-2x fa-share"></i></div>
    </div>
    <span class="font-weight-bold">4. Pilih bentuk/warna tombol (slide for more):</span>
    <div class="button-slider owl-carousel mt-2">
        <a href="#" class="btn btn-m btn-full bg-success color-white text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full bg-success color-white text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full bg-info color-white text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full bg-info color-white text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full bg-danger color-white text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full bg-danger color-white text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full bg-warning color-white text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full bg-warning color-white text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full btn-outline-info text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full btn-outline-info text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full btn-outline-warning text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full btn-outline-warning text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full btn-outline-secondary text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full btn-outline-secondary text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full btn-outline-primary text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full btn-outline-primary text-uppercase font-900')">Button 2</a>
        <a href="#" class="btn btn-m btn-full btn-outline-success text-uppercase font-900" data-menu="customize-button-2" onclick="customize_button('button-2','btn btn-m btn-full btn-outline-success text-uppercase font-900')">Button 2</a>
    </div>
</div>
<div id="menu-crop" class="menu menu-box-modal rounded-m" data-menu-height="50%" data-menu-width="100%" data-menu-effect="menu-over">
    <img class="bg-highlight" id="image-crop" style="max-width: 100%;">
    <a href="#" class="close-menu btn btn-3d btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s border-blue2-dark bg-blue2-light" style="position:absolute; bottom: 0; left:0;">Batal</a>
    <a href="#" class="crop-btn close-menu btn btn-3d btn-m btn-full mb-3 rounded-xs text-uppercase font-900 shadow-s border-red2-dark bg-red2-light" style="position:absolute; bottom: 0; right:0;">Simpan</a>
</div>
<div id="menu-confirm" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="170" data-menu-effect="menu-over" style="height: 200px;">
    <h2 class="text-center font-700 mt-3 pt-1">Apakah anda yakin?</h2>
    <p class="boxed-text-l">
        Anda akan menyimpan promo ini.
    </p>
    <div class="row mr-3 ml-3">
        <div class="col-6">
            <a href="javascript:void(0)" class="btn-simpan-promo btn btn-sm btn-full button-s shadow-l rounded-s text-uppercase font-900 bg-green1-dark"><i class="fa fa-save mr-1"></i> Simpan</a>
        </div>
        <div class="col-6">
            <a href="javascript:void(0)" class="close-menu btn btn-sm btn-full button-s shadow-l rounded-s text-uppercase font-900 bg-red1-dark"><i class="fa fa-times mr-1"></i> Batalkan</a>
        </div>
    </div>
</div>
<div id="menu-success-1" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="270" data-menu-effect="menu-over" style="display: block; height: 305px;">
    <h1 class="text-center mt-4"><i class="fa fa-3x fa-check-circle color-green1-dark"></i></h1>
    <h1 class="text-center mt-3 text-uppercase font-700">Berhasil</h1>
    <p class="boxed-text-l">
        Promo telah berhasil disimpan
    </p>
    <a href="#" class="btn-ok btn btn-m btn-center-m button-s shadow-l rounded-s text-uppercase font-900 bg-green1-light">Oke</a>
</div>
<div id="menu-failed-1" class="menu menu-box-bottom menu-box-detached rounded-m" data-menu-height="270" data-menu-effect="menu-over" style="display: block; height: 305px;">
    <h1 class="text-center mt-4"><i class="fa fa-3x fa-times-circle color-red1-dark"></i></h1>
    <h1 class="text-center mt-3 text-uppercase font-700">Gagal</h1>
    <p class="boxed-text-l">
        Terjadi kesalahan, harap coba kembali.
    </p>
    <a href="#" class="close-menu btn btn-m btn-center-m button-s shadow-l rounded-s text-uppercase font-900 bg-red1-light">Oke</a>
</div>
@endsection