<div class="list-group list-custom-small ps-1 pe-3">
    <a href="javascript:void(0)" onclick="setSort('asc')">
        <i class="font-18 fa fa-sort-alpha-down color-highlight"></i>
        <span class="font-13">Ascending (A-Z)</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a href="javascript:void(0)" onclick="setSort('desc')">
        <i class="font-18 fa fa-sort-alpha-up color-highlight"></i>
        <span class="font-13">Descending (Z-A)</span>
        <i class="fa fa-angle-right"></i>
    </a>
</div>