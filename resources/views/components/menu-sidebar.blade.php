<div class="menu-header">
    <a href="#" data-toggle-theme class="border-right-0"><i class="fa font-12 color-yellow1-dark fa-lightbulb"></i></a>
    <a href="#" data-menu="menu-highlights" class="border-right-0"><i class="fa font-12 color-green1-dark fa-brush"></i></a>
    <a href="#" data-menu="menu-share" class="border-right-0"><i class="fa font-12 color-red2-dark fa-share-alt"></i></a>
    <a href="#" class="border-right-0"><i class="fa font-12 color-red2-dark fa-times"></i></a>
</div>
<div class="menu-logo text-center">
    <a href="#"><img class="rounded-circle bg-highlight" width="80" src="{{ @session()->get('sess_user')['image'] ? session()->get('sess_user')['image'] : '/images/avatars/empty.png' }}"></a>
    <h1 class="pt-3 font-800 font-28 text-uppercase">{{ @session()->get('sess_user')['name'] ? session()->get('sess_user')['name'] : 'Basinergi' }}</h1>
    <p class="font-11 mt-n2">{{ @session()->get('sess_user')['email'] ? session()->get('sess_user')['email'] : 'Selamat datang di basinergi.' }}</p>
</div>

<div class="menu-items">
    <a id="nav-starters" href="{{url('/')}}">
        <i data-feather="home" data-feather-line="1" data-feather-size="21" data-feather-color="blue2-dark" data-feather-bg="blue2-fade-light"></i>
        <span>Home</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <a id="nav-starters" href="{{url('usaha')}}">
        <i data-feather="briefcase" data-feather-line="1" data-feather-size="21" data-feather-color="red2-dark" data-feather-bg="red2-fade-light"></i>
        <span>Daftar Usaha</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <a id="nav-starters" href="{{url('promo')}}">
        <i data-feather="star" data-feather-line="1" data-feather-size="21" data-feather-color="green2-dark" data-feather-bg="green1-fade-light"></i>
        <span>Promo Produk</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <a id="nav-starters" href="{{url('peluang')}}">
        <i data-feather="shopping-bag" data-feather-line="1" data-feather-size="21" data-feather-color="brown2-dark" data-feather-bg="brown1-fade-light"></i>
        <span>Peluang Usaha</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <a id="nav-starters" href="{{url('blog')}}">
        <i data-feather="file-text" data-feather-line="1" data-feather-size="21" data-feather-color="green2-dark" data-feather-bg="green1-fade-light"></i>
        <span>Artikel & Berita</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <a id="nav-starters" href="{{url('lainnya')}}">
        <i data-feather="settings" data-feather-line="1" data-feather-size="21" data-feather-color="gray2-dark" data-feather-bg="gray2-fade-light"></i>
        <span>Menu Lainnya</span>
        <i class="fa fa-arrow-right"></i>
    </a>
    <a href="#" class="close-menu">
        <i data-feather="x" data-feather-line="3" data-feather-size="16" data-feather-color="red2-dark" data-feather-bg="red2-fade-dark"></i>
        <span>Close</span>
        <i class="fa fa-arrow-right"></i>
    </a>
</div>