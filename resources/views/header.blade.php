<!-- header and footer bar go here-->
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="#" class="header-title color-white"> BASINERGI</a>
    <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars color-white"></i></a>
    <a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-dark"><i class="fas fa-sun color-white"></i></a>
    <a href="#" data-toggle-theme class="header-icon header-icon-2 show-on-theme-light"><i class="fas fa-moon color-white"></i></a>
    <a href="#" data-menu="menu-highlights" class="header-icon header-icon-3"><i class="fas fa-brush color-white"></i></a>
    <a href="#" data-menu="menu-share" class="header-icon header-icon-4"><i class="fas fa-share-alt color-white"></i></a>
</div>