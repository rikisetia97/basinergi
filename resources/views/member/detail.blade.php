@extends('template')
@section('title', 'Usaha : '.$member['user']->name)
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('member')}}" class=" color-white header-title header-subtitle">Informasi Member</a>
    <a href="{{url('member')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')
<div class="page-content">
    <div class="page-title page-title-small">
        <h2><a href="{{url('/member')}}"><i class="fa fa-arrow-left"></i></a>Informasi Member</h2>
    </div>
    <div class="card header-card" data-card-height="85">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="card mt-5">
        <div class="menu-logo text-center mt-5">
            <a href="#"><img class="rounded-circle bg-highlight" width="100" src="{{ @$member['user']->user_image ? $member['user']->user_image : '/images/avatars/empty.png' }}"></a>
            <h1 class="pt-3 font-800 font-28 text-uppercase">{{ $member['user']->name }}</h1>
            <p class="text-capitalize text-truncate"><i class="fa fa-map-marker-alt text-danger"></i> {{strtolower($member['user']->address)}} </p>
            <!-- <p class="font-11 mt-n2">{{ @$member['user']->email ? $member['user']->email : '-' }}</p> -->
        </div>
    </div>
    <div class="content">
        <h5 class="float-left font-16 font-500"><i class="fa fa-briefcase"></i> Daftar Usaha</h5>
        <div class="clearfix"></div>
    </div>
    <div class="content">
        <div class="row mb-0">
            @if(count($member['business']) > 0)
            @foreach($member['business'] as $item)
            <div class="col-12 mb-3">
                <a href="{{url('usaha/'.$item->slug)}}" class="color-highlight">
                    <div class="item bg-theme rounded-m shadow-l">
                        <div class="row p-2 mb-0">
                            <div class="col-5 col-md-3">
                                <img class="rounded-sm" src="{{$item->logo}}" style="width: 100%; height:100%">
                            </div>
                            <div class="col-5 col-md-7 text-left" style="margin-left: -15px;">
                                <h5 class="text-truncate">{{$item->business_name}}</h5>
                                <span class="opacity-60"><i class="fa {{$item->icon}}"></i> {{$item->category_name}}</span>
                                <p class="text-capitalize text-truncate"><i class="fa fa-map-marker-alt text-danger"></i> {{strtolower($item->location)}} </p>
                            </div>
                            <div class="col-2 text-right">
                                <i class="fa fa-angle-right fa-2x mt-4"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            @else
            <div class="col-12 mb-3 card p-3">
                <span>Tidak memiliki usaha</span>
            </div>
            @endif
        </div>
    </div>

</div>
@endsection