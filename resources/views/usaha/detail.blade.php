@extends('template')
@section('title', $usaha->business_name)
@section('header')
<div class="header header-fixed header-auto-show header-logo-app bg-highlight">
    <a href="{{url('usaha')}}" class=" color-white header-title header-subtitle">Informasi Usaha</a>
    <a href="{{url('usaha')}}" class="header-icon header-icon-1  color-white"><i class="fas fa-arrow-left"></i></a>
    <a href="#" class="header-icon header-icon-2"><i class="fas fa-question-circle color-white"></i></a>
</div>
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-small">
        <h2><a href="{{url('/usaha')}}"><i class="fa fa-arrow-left"></i></a> Informasi Usaha</h2>
    </div>
    <div class="card header-card" data-card-height="85">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>
    <div class="card preload-img mb-0" data-src="{{$usaha->image}}" data-card-height="27vh"> </div>

    <div class="card mt-1 mb-0">
        <div class="d-flex content mb-1">
            <!-- left side of profile -->
            <div class="flex-grow-1">
                <h1 class="font-700">{{$usaha->business_name}} </h1>
                <p class="mb-1 color-highlight">
                    <i class="fa {{$usaha->icon}} mr-1"></i> {{$usaha->category_name}}
                </p>
                <p class="font-13 text-capitalize">
                    <i class="fa fa-map-marker-alt text-danger mr-1"></i> {{strtolower($usaha->location)}}
                </p>
            </div>
        </div>
        <div class="divider mb-0 mt-0"></div>
        <div class="p-3">
            <button class="btn btn-success btn-block btn-rounded" data-menu="menu-share"><i class="fa fa-share-alt mr-1"></i> Bagikan</button>
        </div>
        <div class="divider mb-0 mt-0"></div>
        <div class="p-3">
            <h5>Deskripsi Usaha</h5>
            <!-- Content -->
            <?php echo $usaha->business_post; ?>
            <h5>Gallery (click to zoom)</h5>

            <!-- Content -->
            @if($usaha->additional_post)
            @php
            $usaha->additional_post = explode(',',$usaha->additional_post);
            @endphp
            <div class="row text-center row-cols-3 mb-0">
                @foreach($usaha->additional_post as $item)
                <a class="mb-2 col default-link" data-lightbox="gallery-1" href="{{$item}}">
                    <img src="{{$item}}" data-src="{{$item}}" class="preload-img img-fluid rounded-xs" alt="img">
                </a>
                @endforeach
            </div>
            @else
            <span>Galeri tidak ditemukan</span>
            @endif
        </div>
        <div class="divider mb-3 mt-1"></div>
        <div class="content mt-0">
            <div class="row">
                @if($usaha->custom_button_2)
                <div class="col-6">
                    <?php echo str_replace('data-menu="customize-button-1"', '', $usaha->custom_button_1); ?>
                </div>
                <div class="col-6">
                    <?php echo str_replace('data-menu="customize-button-2"', '', $usaha->custom_button_2); ?>
                </div>
                @else
                <div class="col-12">
                    <?php echo str_replace('data-menu="customize-button-1"', '', $usaha->custom_button_1); ?>
                </div>
                @endif
            </div>
        </div>
        <div class="divider b mb-0 mt-0"></div>
        <div class="d-flex p-4 mb-0">
            <div class="w-50 border-right text-center pr-3">
                <img data-src="{{$usaha->image_user}}" width="100" class="preload-img rounded-circle">
                <h6 class="font-14 font-600 mt-2 text-center mb-2">{{$usaha->name}}</h6>
                <p class="text-muted mt-n3 font-9 font-400 text-center mb-0 pb-0">Pemilik</p>
            </div>
            <div class="w-50 pl-3 pt-0 mt-3">
                <h4>Angkatan {{$usaha->class_year}}</h4>
                <p class="text-capitalize text-truncate mb-3"><i class="fa fa-map-marker-alt text-danger"></i> {{strtolower($usaha->address)}} </p>
                <a href="{{url('member/'.$usaha->user_slug)}}" class="btn btn-s border-secondary color-secondary text-uppercase">Lihat Profile <i class="fa fa-arrow-right ml-1"></i></a>
            </div>
        </div>
    </div>

</div>
@endsection