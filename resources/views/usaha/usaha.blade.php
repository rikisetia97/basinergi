@extends('template')
@section('title', 'Usaha')
@section('header')
@include('header')
@endsection
@section('footer')
@include('footer')
@endsection
@section('content')
<div class="page-content">

    <div class="page-title page-title-large mb-0">
        <h2 class="greeting-text usaha-text"></h2>
    </div>
    <div class="card header-card" data-card-height="75">
        <div class="card-overlay bg-highlight opacity-95"></div>
        <div class="card-overlay dark-mode-tint"></div>
        <div class="card-bg preload-img" data-src="{{url('images/pictures/20s.jpg')}}"></div>
    </div>

    <!-- Kategori -->
    <div class="content mt-5">
        <h5 class="float-left font-16 font-600">Kategori Usaha</h5>
        <div class="clearfix"></div>
    </div>
    <div class="kategori-list-slider owl-carousel mt-0 mb-n1">
        <a href="#" data-card-height="80" class="card card-style mb-4 mx-0 bg-highlight cat_header cat_header_" onclick="setFilter('')">
            <div class="card-center text-center">
                <i class="fa fa-th-large font-15 color-white cat_icon cat_icon_"></i><br>
                <span class="text-dark font-weight-bold mb-0 color-white cat_name cat_name_" style="font-size: 12px;"> Semua</span>
            </div>
        </a>
        @foreach($kategori as $item)
        <a href="#" data-card-height="80" class="card card-style mb-4 mx-0 cat_header cat_header_{{$item->id}}" onclick="setFilter('{{$item->id}}')">
            <div class="card-center text-center">
                <i class="fa {{$item->icon}} font-15 color-highlight cat_icon cat_icon_{{$item->id}}"></i><br>
                <span class="text-dark font-weight-bold mb-0 cat_name cat_name_{{$item->id}}" style="font-size: 12px;"> {{$item->category_name}}</span>
            </div>
        </a>
        @endforeach
    </div>
    <div class="content mt-0">
        <div class="search-box bg-theme rounded-m shadow-s bottom-0">
            <i class="fa fa-search"></i>
            <input type="text" class="border-0 search-bar" placeholder="Cari berbagai jenis usaha disini ..." data-search>
        </div>
    </div>
    <div class="content">
        <div class="row mb-0">
            <div class="col-6 text-left">
                <a href="javascript:void(0)" class="mr-2 icon icon-xs rounded-xs bg-theme text-dark shadow-l" data-menu="menu-share-list"><i class="fa fa-sort"></i> <span class="ml-0">Sort by</span><i class="fa fa-caret-down "></i></a>
            </div>
            <div class="col-6 text-right">
                <a onclick="setList('list')" href="#" class="setList setList_list mr-2 icon icon-xs rounded-xs bg-highlight text-light shadow-l"><i class="fa fa-th-list"></i></a>
                <a onclick="setList('square')" href="#" class="setList setList_square icon icon-xs rounded-xs bg-theme text-dark shadow-l"><i class="fa fa-th-large"></i></a>
            </div>
        </div>
    </div>
    <div class="content list_usaha row mb-0 mx-0">

    </div>

</div>

@endsection